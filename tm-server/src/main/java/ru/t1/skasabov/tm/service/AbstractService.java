package ru.t1.skasabov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.skasabov.tm.api.repository.IRepository;
import ru.t1.skasabov.tm.api.service.IConnectionService;
import ru.t1.skasabov.tm.api.service.IService;
import ru.t1.skasabov.tm.exception.entity.ModelEmptyException;
import ru.t1.skasabov.tm.exception.field.IdEmptyException;
import ru.t1.skasabov.tm.exception.field.IndexIncorrectException;
import ru.t1.skasabov.tm.model.AbstractModel;

import java.sql.Connection;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractService<M extends AbstractModel> implements IService<M> {

    @NotNull
    protected final IConnectionService connectionService;

    public AbstractService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @NotNull
    protected Connection getConnection() {
        return connectionService.getConnection();
    }

    @NotNull
    protected abstract IRepository<M> getRepository(@NotNull final Connection connection);

    @Override
    @SneakyThrows
    public void removeAll(@Nullable final Collection<M> collection) {
        if (collection == null) return;
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IRepository<M> repository = getRepository(connection);
            repository.removeAll(collection);
            connection.commit();
        }
        catch(@NotNull final Exception e) {
            connection.rollback();
            throw e;
        }
        finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public M add(@Nullable final M model) {
        if (model == null) throw new ModelEmptyException();
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IRepository<M> repository = getRepository(connection);
            repository.add(model);
            connection.commit();
        }
        catch(@NotNull final Exception e) {
            connection.rollback();
            throw e;
        }
        finally {
            connection.close();
        }
        return model;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Collection<M> addAll(@Nullable final Collection<M> models) {
        if (models == null) return Collections.emptyList();
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IRepository<M> repository = getRepository(connection);
            repository.addAll(models);
            connection.commit();
        }
        catch(@NotNull final Exception e) {
            connection.rollback();
            throw e;
        }
        finally {
            connection.close();
        }
        return models;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Collection<M> set(@Nullable final Collection<M> models) {
        if (models == null) return Collections.emptyList();
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IRepository<M> repository = getRepository(connection);
            repository.set(models);
            connection.commit();
        }
        catch(@NotNull final Exception e) {
            connection.rollback();
            throw e;
        }
        finally {
            connection.close();
        }
        return models;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<M> findAll() {
        try(@NotNull final Connection connection = getConnection()) {
            @NotNull final IRepository<M> repository = getRepository(connection);
            return repository.findAll();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<M> findAll(@Nullable final Comparator<M> comparator) {
        if (comparator == null) return findAll();
        try(@NotNull final Connection connection = getConnection()) {
            @NotNull final IRepository<M> repository = getRepository(connection);
            return repository.findAll(comparator);
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public M findOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        try(@NotNull final Connection connection = getConnection()) {
            @NotNull final IRepository<M> repository = getRepository(connection);
            return repository.findOneById(id);
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public M findOneByIndex(@Nullable final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index >= getSize()) throw new IndexIncorrectException();
        try(@NotNull final Connection connection = getConnection()) {
            @NotNull final IRepository<M> repository = getRepository(connection);
            return repository.findOneByIndex(index);
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public M removeOne(@Nullable final M model) {
        if (model == null) throw new ModelEmptyException();
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IRepository<M> repository = getRepository(connection);
            repository.removeOne(model);
            connection.commit();
        }
        catch(@NotNull final Exception e) {
            connection.rollback();
            throw e;
        }
        finally {
            connection.close();
        }
        return model;
    }

    @Nullable
    @Override
    @SneakyThrows
    public M removeOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final Connection connection = getConnection();
        M model;
        try {
            @NotNull final IRepository<M> repository = getRepository(connection);
            model = repository.removeOneById(id);
            connection.commit();
        }
        catch(@NotNull final Exception e) {
            connection.rollback();
            throw e;
        }
        finally {
            connection.close();
        }
        return model;
    }

    @Nullable
    @Override
    @SneakyThrows
    public M removeOneByIndex(@Nullable final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index >= getSize()) throw new IndexIncorrectException();
        @NotNull final Connection connection = getConnection();
        M model;
        try {
            @NotNull final IRepository<M> repository = getRepository(connection);
            model = repository.removeOneByIndex(index);
            connection.commit();
        }
        catch(@NotNull final Exception e) {
            connection.rollback();
            throw e;
        }
        finally {
            connection.close();
        }
        return model;
    }

    @Override
    @SneakyThrows
    public void removeAll() {
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IRepository<M> repository = getRepository(connection);
            repository.removeAll();
            connection.commit();
        }
        catch(@NotNull final Exception e) {
            connection.rollback();
            throw e;
        }
        finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public int getSize() {
        try(@NotNull final Connection connection = getConnection()) {
            @NotNull final IRepository<M> repository = getRepository(connection);
            return repository.getSize();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public Boolean existsById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        try(@NotNull final Connection connection = getConnection()) {
            @NotNull final IRepository<M> repository = getRepository(connection);
            return repository.existsById(id);
        }
    }

}
