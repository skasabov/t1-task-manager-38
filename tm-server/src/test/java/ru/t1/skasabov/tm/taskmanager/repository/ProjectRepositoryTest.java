package ru.t1.skasabov.tm.taskmanager.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.t1.skasabov.tm.api.repository.IProjectRepository;
import ru.t1.skasabov.tm.api.repository.ITaskRepository;
import ru.t1.skasabov.tm.api.repository.IUserRepository;
import ru.t1.skasabov.tm.api.service.*;
import ru.t1.skasabov.tm.enumerated.ProjectSort;
import ru.t1.skasabov.tm.model.Project;
import ru.t1.skasabov.tm.model.Task;
import ru.t1.skasabov.tm.repository.ProjectRepository;
import ru.t1.skasabov.tm.repository.TaskRepository;
import ru.t1.skasabov.tm.repository.UserRepository;
import ru.t1.skasabov.tm.service.*;

import java.sql.Connection;
import java.util.*;

public class ProjectRepositoryTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    @NotNull
    private static String USER_ID_ONE;

    @NotNull
    private static String USER_ID_TWO;

    @NotNull
    private List<Project> projects;

    @NotNull
    private List<Task> tasks;

    @NotNull
    private IProjectRepository projectRepository;

    @NotNull
    private ITaskRepository taskRepository;

    @NotNull
    private IUserRepository userRepository;

    @NotNull
    private Connection connection;

    @Before
    @SneakyThrows
    public void initRepository() {
        @NotNull final IPropertyService propertyService = new PropertyService();
        @NotNull final IConnectionService connectionService = new ConnectionService(propertyService);
        connection = connectionService.getConnection();
        connection.setAutoCommit(true);
        userRepository = new UserRepository(connection);
        taskRepository = new TaskRepository(connection);
        projectRepository = new ProjectRepository(connection);
        projects = projectRepository.findAll();
        tasks = new ArrayList<>();
        for (@NotNull final Project project : projects) {
            Assert.assertNotNull(project.getUserId());
            tasks.addAll(taskRepository.findAllByProjectId(project.getUserId(), project.getId()));
        }
        projectRepository.removeAll();
        USER_ID_ONE = userRepository.create("user_one", "user_one").getId();
        USER_ID_TWO = userRepository.create("user_two", "user_two").getId();
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final Project project = new Project();
            project.setName("Test " + i);
            if (i <= 5) project.setUserId(USER_ID_ONE);
            else project.setUserId(USER_ID_TWO);
            projectRepository.add(project);
        }
    }

    @Test
    public void testCreateName() {
        final int expectedNumberOfEntries = projectRepository.getSize() + 1;
        @NotNull final String name = "Test Project";
        @NotNull final Project actualProject = projectRepository.create(USER_ID_ONE, name, "");
        Assert.assertEquals(expectedNumberOfEntries, projectRepository.getSize());
        Assert.assertEquals(USER_ID_ONE, actualProject.getUserId());
        Assert.assertEquals(name, actualProject.getName());
    }

    @Test
    public void testCreateDescription() {
        final int expectedNumberOfEntries = projectRepository.getSize() + 1;
        @NotNull final String name = "Test Project";
        @NotNull final String description = "Test Description";
        @NotNull final Project actualProject = projectRepository.create(USER_ID_TWO, name, description);
        Assert.assertEquals(expectedNumberOfEntries, projectRepository.getSize());
        Assert.assertEquals(USER_ID_TWO, actualProject.getUserId());
        Assert.assertEquals(name, actualProject.getName());
        Assert.assertEquals(description, actualProject.getDescription());
    }

    @Test
    public void createProject() {
        final int expectedNumberOfEntries = projectRepository.getSize() + 1;
        @NotNull final String name = "Test Project";
        @NotNull final String description = "Test Description";
        @NotNull final Date dateBegin = new Date();
        @NotNull final Date dateEnd = new Date();
        @NotNull final Project actualProject = projectRepository.create(
                USER_ID_ONE, name, description, dateBegin, dateEnd
        );
        Assert.assertEquals(expectedNumberOfEntries, projectRepository.getSize());
        Assert.assertEquals(USER_ID_ONE, actualProject.getUserId());
        Assert.assertEquals(name, actualProject.getName());
        Assert.assertEquals(description, actualProject.getDescription());
        Assert.assertEquals(dateBegin, actualProject.getDateBegin());
        Assert.assertEquals(dateEnd, actualProject.getDateEnd());
    }

    @Test
    public void testUpdate() {
        @NotNull final Project project = projectRepository.findAll(USER_ID_TWO).get(0);
        project.setName("Test Project One");
        project.setDescription("Test Description One");
        @NotNull final Project actualProject = projectRepository.update(project);
        Assert.assertEquals(USER_ID_TWO, project.getUserId());
        Assert.assertEquals("Test Project One", actualProject.getName());
        Assert.assertEquals("Test Description One", actualProject.getDescription());
    }

    @Test
    public void testAdd() {
        final int expectedNumberOfEntries = projectRepository.getSize() + 1;
        @NotNull final Project project = new Project();
        project.setName("Test Project");
        project.setUserId(USER_ID_TWO);
        projectRepository.add(project);
        Assert.assertEquals(expectedNumberOfEntries, projectRepository.getSize());
    }

    @Test
    public void testAddForUser() {
        final int expectedNumberOfEntries = projectRepository.getSize(USER_ID_TWO) + 1;
        @NotNull final Project project = new Project();
        project.setName("Test Project");
        project.setUserId(USER_ID_TWO);
        projectRepository.add(USER_ID_TWO, project);
        Assert.assertEquals(expectedNumberOfEntries, projectRepository.getSize(USER_ID_TWO));
    }

    @Test
    public void testAddAll() {
        final int expectedNumberOfEntries = projectRepository.getSize() + 4;
        @NotNull final List<Project> actualProjects = new ArrayList<>();
        for (int i = 1; i <= 4; i++) {
            @NotNull final Project project = new Project();
            project.setName("Test Project " + i);
            project.setUserId(USER_ID_ONE);
            actualProjects.add(project);
        }
        projectRepository.addAll(actualProjects);
        Assert.assertEquals(expectedNumberOfEntries, projectRepository.getSize());
    }

    @Test
    public void testSet() {
        @NotNull final List<Project> actualProjects = new ArrayList<>();
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final Project project = new Project();
            project.setName("Test Project " + i);
            project.setUserId(USER_ID_TWO);
            actualProjects.add(project);
        }
        projectRepository.set(actualProjects);
        Assert.assertEquals(NUMBER_OF_ENTRIES, projectRepository.getSize());
    }

    @Test
    public void testClearAll() {
        projectRepository.removeAll();
        Assert.assertEquals(0, projectRepository.getSize());
    }

    @Test
    public void testClearAllForUser() {
        final int expectedNumberOfEntries = projectRepository.getSize() - NUMBER_OF_ENTRIES / 2;
        projectRepository.removeAll(USER_ID_ONE);
        Assert.assertEquals(expectedNumberOfEntries, projectRepository.getSize());
    }

    @Test
    public void testClear() {
        final int expectedNumberOfEntries = projectRepository.getSize() - NUMBER_OF_ENTRIES / 2;
        @NotNull final List<Project> projectList = projectRepository.findAll(USER_ID_TWO);
        projectRepository.removeAll(projectList);
        Assert.assertEquals(expectedNumberOfEntries, projectRepository.getSize());
    }

    @Test
    public void testFindAll() {
        @NotNull final List<Project> projectList = projectRepository.findAll();
        Assert.assertEquals(projectList.size(), projectRepository.getSize());
    }

    @Test
    public void testFindAllWithComparator() {
        @NotNull final ProjectSort sort = ProjectSort.BY_NAME;
        @NotNull final List<Project> projectSortList = projectRepository.findAll(sort.getComparator());
        @NotNull final List<Project> actualProjects = new ArrayList<>();
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final Project project = new Project();
            project.setName("Test " + i);
            actualProjects.add(project);
        }
        actualProjects.add(1, actualProjects.get(NUMBER_OF_ENTRIES - 1));
        actualProjects.remove(NUMBER_OF_ENTRIES);
        Assert.assertEquals(actualProjects, projectSortList);
    }

    @Test
    public void testFindAllWithEmptyComparator() {
        @NotNull final List<Project> projectList = projectRepository.findAll();
        @NotNull final List<Project> projectSortList = projectRepository.findAll((Comparator<Project>) null);
        Assert.assertEquals(projectList, projectSortList);
    }

    @Test
    public void testFindAllForUser() {
        @NotNull final List<Project> projectList = projectRepository.findAll(USER_ID_ONE);
        Assert.assertEquals(NUMBER_OF_ENTRIES / 2, projectList.size());
    }

    @Test
    public void testFindAllWithComparatorForUser() {
        @NotNull final ProjectSort sort = ProjectSort.BY_NAME;
        @NotNull final List<Project> projectSortList = projectRepository.findAll(USER_ID_TWO, sort.getComparator());
        @NotNull final List<Project> actualProjects = new ArrayList<>();
        for (int i = 1; i <= NUMBER_OF_ENTRIES / 2; i++) {
            @NotNull final Project project = new Project();
            project.setName("Test " + (i + 5));
            actualProjects.add(project);
        }
        actualProjects.add(0, actualProjects.get(NUMBER_OF_ENTRIES / 2 - 1));
        actualProjects.remove(NUMBER_OF_ENTRIES / 2);
        Assert.assertEquals(actualProjects, projectSortList);
    }

    @Test
    public void testFindAllWithEmptyComparatorForUser() {
        @NotNull final List<Project> projectList = projectRepository.findAll(USER_ID_TWO);
        @NotNull final List<Project> projectSortList = projectRepository.findAll(USER_ID_TWO, null);
        Assert.assertEquals(projectList, projectSortList);
    }

    @Test
    public void testFindById() {
        @NotNull final Project project = projectRepository.findAll().get(0);
        @NotNull final String projectId = projectRepository.findAll().get(0).getId();
        @Nullable final Project actualProject = projectRepository.findOneById(projectId);
        Assert.assertNotNull(actualProject);
        Assert.assertEquals(project.getName(), actualProject.getName());
        Assert.assertEquals(project.getDescription(), actualProject.getDescription());
        Assert.assertEquals(project.getUserId(), actualProject.getUserId());
    }

    @Test
    public void testFindByIdForUser() {
        @NotNull final Project project = projectRepository.findAll(USER_ID_ONE).get(0);
        @NotNull final String projectId = projectRepository.findAll(USER_ID_ONE).get(0).getId();
        @Nullable final Project actualProject = projectRepository.findOneById(USER_ID_ONE, projectId);
        Assert.assertNotNull(actualProject);
        Assert.assertEquals(project.getName(), actualProject.getName());
        Assert.assertEquals(project.getDescription(), actualProject.getDescription());
        Assert.assertEquals(project.getUserId(), actualProject.getUserId());
    }

    @Test
    public void testFindByIdProjectNotFound() {
        @NotNull final String id = UUID.randomUUID().toString();
        Assert.assertNull(projectRepository.findOneById(id));
    }

    @Test
    public void testFindByIdProjectNotFoundForUser() {
        @NotNull final String id = UUID.randomUUID().toString();
        Assert.assertNull(projectRepository.findOneById(USER_ID_ONE, id));
    }

    @Test
    public void testFindByIndex() {
        @NotNull final Project project = projectRepository.findAll().get(0);
        @Nullable final Project actualProject = projectRepository.findOneByIndex(1);
        Assert.assertNotNull(actualProject);
        Assert.assertEquals(project.getName(), actualProject.getName());
        Assert.assertEquals(project.getDescription(), actualProject.getDescription());
        Assert.assertEquals(project.getUserId(), actualProject.getUserId());
    }

    @Test
    public void testFindByIndexProjectNotFound() {
        projectRepository.removeAll();
        Assert.assertNull(projectRepository.findOneByIndex(1));
    }

    @Test
    public void testFindByIndexForUser() {
        @NotNull final Project project = projectRepository.findAll(USER_ID_TWO).get(0);
        @Nullable final Project actualProject = projectRepository.findOneByIndex(USER_ID_TWO, 1);
        Assert.assertNotNull(actualProject);
        Assert.assertEquals(project.getName(), actualProject.getName());
        Assert.assertEquals(project.getDescription(), actualProject.getDescription());
        Assert.assertEquals(project.getUserId(), actualProject.getUserId());
    }

    @Test
    public void testFindByIndexForUserProjectNotFound() {
        projectRepository.removeAll();
        Assert.assertNull(projectRepository.findOneByIndex(USER_ID_ONE, 1));
    }

    @Test
    public void testGetSize() {
        final int expectedNumberOfEntries = projectRepository.getSize() + 1;
        @NotNull final Project project = new Project();
        project.setName("Test Project");
        project.setUserId(USER_ID_ONE);
        projectRepository.add(project);
        Assert.assertEquals(expectedNumberOfEntries, projectRepository.getSize());
    }

    @Test
    public void testGetSizeForUser() {
        @NotNull final Project project = new Project();
        project.setName("Test Project");
        project.setUserId(USER_ID_TWO);
        projectRepository.add(project);
        Assert.assertEquals(NUMBER_OF_ENTRIES / 2 + 1, projectRepository.getSize(USER_ID_TWO));
    }

    @Test
    public void testIsNotFoundById() {
        @NotNull final String validId = projectRepository.findAll().get(0).getId();
        @NotNull final String invalidId = UUID.randomUUID().toString();
        Assert.assertFalse(projectRepository.existsById(invalidId));
        Assert.assertTrue(projectRepository.existsById(validId));
    }

    @Test
    public void testIsNotFoundByIdForUser() {
        @NotNull final String validId = projectRepository.findAll(USER_ID_ONE).get(0).getId();
        @NotNull final String invalidId = UUID.randomUUID().toString();
        Assert.assertFalse(projectRepository.existsById(USER_ID_ONE, invalidId));
        Assert.assertTrue(projectRepository.existsById(USER_ID_ONE, validId));
    }

    @Test
    public void testRemove() {
        final int expectedNumberOfEntries = projectRepository.getSize() - 1;
        @NotNull final Project project = projectRepository.findAll().get(0);
        projectRepository.removeOne(project);
        Assert.assertEquals(expectedNumberOfEntries, projectRepository.getSize());
    }

    @Test
    public void testRemoveForUser() {
        final int expectedNumberOfEntries = projectRepository.getSize(USER_ID_TWO) - 1;
        @NotNull final Project project = projectRepository.findAll(USER_ID_TWO).get(0);
        projectRepository.removeOne(USER_ID_TWO, project);
        Assert.assertEquals(expectedNumberOfEntries, projectRepository.getSize(USER_ID_TWO));
    }

    @Test
    public void testRemoveById() {
        final int expectedNumberOfEntries = projectRepository.getSize() - 1;
        @NotNull final String projectId = projectRepository.findAll().get(0).getId();
        projectRepository.removeOneById(projectId);
        Assert.assertEquals(expectedNumberOfEntries, projectRepository.getSize());
    }

    @Test
    public void testRemoveByIdForUser() {
        final int expectedNumberOfEntries = projectRepository.getSize(USER_ID_ONE) - 1;
        @NotNull final String projectId = projectRepository.findAll(USER_ID_ONE).get(0).getId();
        projectRepository.removeOneById(USER_ID_ONE, projectId);
        Assert.assertEquals(expectedNumberOfEntries, projectRepository.getSize(USER_ID_ONE));
    }

    @Test
    public void testRemoveByIndex() {
        final int expectedNumberOfEntries = projectRepository.getSize() - 1;
        projectRepository.removeOneByIndex(1);
        Assert.assertEquals(expectedNumberOfEntries, projectRepository.getSize());
    }

    @Test
    public void testRemoveByIndexProjectNotFound() {
        projectRepository.removeAll();
        Assert.assertNull(projectRepository.removeOneByIndex(1));
    }

    @Test
    public void testRemoveByIndexForUser() {
        final int expectedNumberOfEntries = projectRepository.getSize(USER_ID_TWO) - 1;
        projectRepository.removeOneByIndex(USER_ID_TWO, 1);
        Assert.assertEquals(expectedNumberOfEntries, projectRepository.getSize(USER_ID_TWO));
    }

    @Test
    public void testRemoveByIndexForUserProjectNotFound() {
        projectRepository.removeAll();
        Assert.assertNull(projectRepository.removeOneByIndex(USER_ID_ONE, 1));
    }

    @After
    @SneakyThrows
    public void clearRepository() {
        projectRepository.set(projects);
        taskRepository.addAll(tasks);
        userRepository.removeOneById(USER_ID_ONE);
        userRepository.removeOneById(USER_ID_TWO);
        connection.close();
    }

}
