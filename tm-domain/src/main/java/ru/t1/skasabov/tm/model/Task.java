package ru.t1.skasabov.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.skasabov.tm.api.model.IWBS;
import ru.t1.skasabov.tm.enumerated.Status;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public final class Task extends AbstractUserOwnedModel implements IWBS {

    @Nullable
    private String name;

    @Nullable
    private String description = "";

    @NotNull
    private Status status = Status.NOT_STARTED;

    @Nullable
    private String projectId;

    @NotNull
    private Date created = new Date();

    @Nullable
    private Date dateBegin;

    @Nullable
    private Date dateEnd;

    @Override
    public boolean equals(@Nullable final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        @NotNull final Task task = (Task) o;
        return name != null ? name.equals(task.getName()) : task.getName() == null;
    }

    @Override
    public int hashCode() {
        return name != null ? 31 * name.hashCode() : 0;
    }

    @NotNull
    @Override
    public String toString() {
        return name + " : " + description;
    }

}
