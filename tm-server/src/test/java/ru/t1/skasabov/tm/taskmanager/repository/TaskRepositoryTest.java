package ru.t1.skasabov.tm.taskmanager.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.t1.skasabov.tm.api.repository.IProjectRepository;
import ru.t1.skasabov.tm.api.repository.ITaskRepository;
import ru.t1.skasabov.tm.api.repository.IUserRepository;
import ru.t1.skasabov.tm.api.service.*;
import ru.t1.skasabov.tm.enumerated.TaskSort;
import ru.t1.skasabov.tm.model.Project;
import ru.t1.skasabov.tm.model.Task;
import ru.t1.skasabov.tm.repository.ProjectRepository;
import ru.t1.skasabov.tm.repository.TaskRepository;
import ru.t1.skasabov.tm.repository.UserRepository;
import ru.t1.skasabov.tm.service.*;

import java.sql.Connection;
import java.util.*;

public class TaskRepositoryTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    @NotNull
    private static String USER_ID_ONE;

    @NotNull
    private static String USER_ID_TWO;

    @NotNull
    private Project projectOne;

    @NotNull
    private Project projectTwo;

    @NotNull
    private List<Task> tasks;

    @NotNull
    private IProjectRepository projectRepository;

    @NotNull
    private IUserRepository userRepository;

    @NotNull
    private ITaskRepository taskRepository;

    @NotNull
    private Connection connection;

    @Before
    @SneakyThrows
    public void initRepository() {
        @NotNull final IPropertyService propertyService = new PropertyService();
        @NotNull final IConnectionService connectionService = new ConnectionService(propertyService);
        connection = connectionService.getConnection();
        connection.setAutoCommit(true);
        projectRepository = new ProjectRepository(connection);
        userRepository = new UserRepository(connection);
        taskRepository = new TaskRepository(connection);
        tasks = taskRepository.findAll();
        taskRepository.removeAll();
        USER_ID_ONE = userRepository.create("user_one", "user_one").getId();
        USER_ID_TWO = userRepository.create("user_two", "user_two").getId();
        projectOne = projectRepository.create(USER_ID_ONE, "project_one");
        projectTwo = projectRepository.create(USER_ID_TWO, "project_two");
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final Task task = new Task();
            task.setName("Test " + i);
            if (i <= 5) {
                task.setUserId(USER_ID_ONE);
                task.setProjectId(projectOne.getId());
            } else {
                task.setUserId(USER_ID_TWO);
                task.setProjectId(projectTwo.getId());
            }
            taskRepository.add(task);
        }
    }

    @Test
    public void testCreateName() {
        final int expectedNumberOfEntries = taskRepository.getSize() + 1;
        @NotNull final String name = "Test Task";
        @NotNull final Task actualTask = taskRepository.create(USER_ID_ONE, name, "");
        Assert.assertEquals(expectedNumberOfEntries, taskRepository.getSize());
        Assert.assertEquals(USER_ID_ONE, actualTask.getUserId());
        Assert.assertEquals(name, actualTask.getName());
    }

    @Test
    public void testCreateDescription() {
        final int expectedNumberOfEntries = taskRepository.getSize() + 1;
        @NotNull final String name = "Test Task";
        @NotNull final String description = "Test Description";
        @NotNull final Task actualTask = taskRepository.create(USER_ID_TWO, name, description);
        Assert.assertEquals(expectedNumberOfEntries, taskRepository.getSize());
        Assert.assertEquals(USER_ID_TWO, actualTask.getUserId());
        Assert.assertEquals(name, actualTask.getName());
        Assert.assertEquals(description, actualTask.getDescription());
    }

    @Test
    public void createTask() {
        final int expectedNumberOfEntries = taskRepository.getSize() + 1;
        @NotNull final String name = "Test Task";
        @NotNull final String description = "Test Description";
        @NotNull final Date dateBegin = new Date();
        @NotNull final Date dateEnd = new Date();
        @NotNull final Task actualTask = taskRepository.create(
                USER_ID_ONE, name, description, dateBegin, dateEnd
        );
        Assert.assertEquals(expectedNumberOfEntries, taskRepository.getSize());
        Assert.assertEquals(USER_ID_ONE, actualTask.getUserId());
        Assert.assertEquals(name, actualTask.getName());
        Assert.assertEquals(description, actualTask.getDescription());
        Assert.assertEquals(dateBegin, actualTask.getDateBegin());
        Assert.assertEquals(dateEnd, actualTask.getDateEnd());
    }

    @Test
    public void testUpdate() {
        @NotNull final Task task = taskRepository.findAll(USER_ID_TWO).get(0);
        task.setName("Test Task One");
        task.setDescription("Test Description One");
        @NotNull final Task actualTask = taskRepository.update(task);
        Assert.assertEquals(USER_ID_TWO, actualTask.getUserId());
        Assert.assertEquals("Test Task One", actualTask.getName());
        Assert.assertEquals("Test Description One", actualTask.getDescription());
    }

    @Test
    public void testAdd() {
        final int expectedNumberOfEntries = taskRepository.getSize() + 1;
        @NotNull final Task task = new Task();
        task.setName("Test Task");
        task.setUserId(USER_ID_TWO);
        taskRepository.add(task);
        Assert.assertEquals(expectedNumberOfEntries, taskRepository.getSize());
    }

    @Test
    public void testAddForUser() {
        final int expectedNumberOfEntries = taskRepository.getSize(USER_ID_TWO) + 1;
        @NotNull final Task task = new Task();
        task.setName("Test Task");
        task.setUserId(USER_ID_TWO);
        taskRepository.add(task);
        Assert.assertEquals(expectedNumberOfEntries, taskRepository.getSize(USER_ID_TWO));
    }

    @Test
    public void testAddAll() {
        final int expectedNumberOfEntries = taskRepository.getSize() + 4;
        @NotNull final List<Task> actualTasks = new ArrayList<>();
        for (int i = 1; i <= 4; i++) {
            @NotNull final Task task = new Task();
            task.setName("Test Task " + i);
            task.setUserId(USER_ID_ONE);
            actualTasks.add(task);
        }
        taskRepository.addAll(actualTasks);
        Assert.assertEquals(expectedNumberOfEntries, taskRepository.getSize());
    }

    @Test
    public void testSet() {
        @NotNull final List<Task> actualTasks = new ArrayList<>();
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final Task task = new Task();
            task.setName("Test Task " + i);
            task.setUserId(USER_ID_TWO);
            actualTasks.add(task);
        }
        taskRepository.set(actualTasks);
        Assert.assertEquals(NUMBER_OF_ENTRIES, taskRepository.getSize());
    }

    @Test
    public void testClearAll() {
        taskRepository.removeAll();
        Assert.assertEquals(0, taskRepository.getSize());
    }

    @Test
    public void testClearAllForUser() {
        final int expectedNumberOfEntries = taskRepository.getSize() - NUMBER_OF_ENTRIES / 2;
        taskRepository.removeAll(USER_ID_ONE);
        Assert.assertEquals(expectedNumberOfEntries, taskRepository.getSize());
    }

    @Test
    public void testClear() {
        final int expectedNumberOfEntries = taskRepository.getSize() - NUMBER_OF_ENTRIES / 2;
        @NotNull final List<Task> taskList = taskRepository.findAll(USER_ID_TWO);
        taskRepository.removeAll(taskList);
        Assert.assertEquals(expectedNumberOfEntries, taskRepository.getSize());
    }

    @Test
    public void testFindAll() {
        @NotNull final List<Task> taskList = taskRepository.findAll();
        Assert.assertEquals(taskList.size(), taskRepository.getSize());
    }

    @Test
    public void testFindAllWithComparator() {
        @NotNull final TaskSort sort = TaskSort.BY_NAME;
        @NotNull final List<Task> taskSortList = taskRepository.findAll(sort.getComparator());
        @NotNull final List<Task> actualTasks = new ArrayList<>();
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final Task task = new Task();
            task.setName("Test " + i);
            actualTasks.add(task);
        }
        actualTasks.add(1, actualTasks.get(NUMBER_OF_ENTRIES - 1));
        actualTasks.remove(NUMBER_OF_ENTRIES);
        Assert.assertEquals(actualTasks, taskSortList);
    }

    @Test
    public void testFindAllWithEmptyComparator() {
        @NotNull final List<Task> taskList = taskRepository.findAll();
        @NotNull final List<Task> taskSortList = taskRepository.findAll((Comparator<Task>) null);
        Assert.assertEquals(taskList, taskSortList);
    }

    @Test
    public void testFindAllForUser() {
        @NotNull final List<Task> taskList = taskRepository.findAll(USER_ID_ONE);
        Assert.assertEquals(NUMBER_OF_ENTRIES / 2, taskList.size());
    }

    @Test
    public void testFindAllWithComparatorForUser() {
        @NotNull final TaskSort sort = TaskSort.BY_NAME;
        @NotNull final List<Task> taskSortList = taskRepository.findAll(USER_ID_TWO, sort.getComparator());
        @NotNull final List<Task> actualTasks = new ArrayList<>();
        for (int i = 1; i <= NUMBER_OF_ENTRIES / 2; i++) {
            @NotNull final Task task = new Task();
            task.setName("Test " + (i + 5));
            actualTasks.add(task);
        }
        actualTasks.add(0, actualTasks.get(NUMBER_OF_ENTRIES / 2 - 1));
        actualTasks.remove(NUMBER_OF_ENTRIES / 2);
        Assert.assertEquals(actualTasks, taskSortList);
    }

    @Test
    public void testFindAllWithEmptyComparatorForUser() {
        @NotNull final List<Task> taskList = taskRepository.findAll(USER_ID_TWO);
        @NotNull final List<Task> taskSortList = taskRepository.findAll(USER_ID_TWO, null);
        Assert.assertEquals(taskList, taskSortList);
    }

    @Test
    public void testFindById() {
        @NotNull final Task task = taskRepository.findAll().get(0);
        @NotNull final String taskId = taskRepository.findAll().get(0).getId();
        @Nullable final Task actualTask = taskRepository.findOneById(taskId);
        Assert.assertNotNull(actualTask);
        Assert.assertEquals(task.getName(), actualTask.getName());
        Assert.assertEquals(task.getDescription(), actualTask.getDescription());
        Assert.assertEquals(task.getUserId(), actualTask.getUserId());
    }

    @Test
    public void testFindByIdForUser() {
        @NotNull final Task task = taskRepository.findAll(USER_ID_ONE).get(0);
        @NotNull final String taskId = taskRepository.findAll(USER_ID_ONE).get(0).getId();
        @Nullable final Task actualTask = taskRepository.findOneById(USER_ID_ONE, taskId);
        Assert.assertNotNull(actualTask);
        Assert.assertEquals(task.getName(), actualTask.getName());
        Assert.assertEquals(task.getDescription(), actualTask.getDescription());
        Assert.assertEquals(task.getUserId(), actualTask.getUserId());
    }

    @Test
    public void testFindByIdTaskNotFound() {
        @NotNull final String id = UUID.randomUUID().toString();
        Assert.assertNull(taskRepository.findOneById(id));
    }

    @Test
    public void testFindByIdTaskNotFoundForUser() {
        @NotNull final String id = UUID.randomUUID().toString();
        Assert.assertNull(taskRepository.findOneById(USER_ID_ONE, id));
    }

    @Test
    public void testFindByIndex() {
        @NotNull final Task task = taskRepository.findAll().get(0);
        @Nullable final Task actualTask = taskRepository.findOneByIndex(1);
        Assert.assertNotNull(actualTask);
        Assert.assertEquals(task.getName(), actualTask.getName());
        Assert.assertEquals(task.getDescription(), actualTask.getDescription());
        Assert.assertEquals(task.getUserId(), actualTask.getUserId());
    }

    @Test
    public void testFindByIndexTaskNotFound() {
        taskRepository.removeAll();
        Assert.assertNull(taskRepository.findOneByIndex(1));
    }

    @Test
    public void testFindByIndexForUser() {
        @NotNull final Task task = taskRepository.findAll(USER_ID_TWO).get(0);
        @Nullable final Task actualTask = taskRepository.findOneByIndex(USER_ID_TWO, 1);
        Assert.assertNotNull(actualTask);
        Assert.assertEquals(task.getName(), actualTask.getName());
        Assert.assertEquals(task.getDescription(), actualTask.getDescription());
        Assert.assertEquals(task.getUserId(), actualTask.getUserId());
    }

    @Test
    public void testFindByIndexForUserTaskNotFound() {
        taskRepository.removeAll();
        Assert.assertNull(taskRepository.findOneByIndex(USER_ID_ONE, 1));
    }

    @Test
    public void testGetSize() {
        final int expectedNumberOfEntries = taskRepository.getSize() + 1;
        @NotNull final Task task = new Task();
        task.setName("Test Task");
        task.setUserId(USER_ID_ONE);
        taskRepository.add(task);
        Assert.assertEquals(expectedNumberOfEntries, taskRepository.getSize());
    }

    @Test
    public void testGetSizeForUser() {
        @NotNull final Task task = new Task();
        task.setName("Test Task");
        task.setUserId(USER_ID_TWO);
        taskRepository.add(task);
        Assert.assertEquals(NUMBER_OF_ENTRIES / 2 + 1, taskRepository.getSize(USER_ID_TWO));
    }

    @Test
    public void testIsNotFoundById() {
        @NotNull final String validId = taskRepository.findAll().get(0).getId();
        @NotNull final String invalidId = UUID.randomUUID().toString();
        Assert.assertFalse(taskRepository.existsById(invalidId));
        Assert.assertTrue(taskRepository.existsById(validId));
    }

    @Test
    public void testIsNotFoundByIdForUser() {
        @NotNull final String validId = taskRepository.findAll(USER_ID_ONE).get(0).getId();
        @NotNull final String invalidId = UUID.randomUUID().toString();
        Assert.assertFalse(taskRepository.existsById(USER_ID_ONE, invalidId));
        Assert.assertTrue(taskRepository.existsById(USER_ID_ONE, validId));
    }

    @Test
    public void testRemove() {
        final int expectedNumberOfEntries = taskRepository.getSize() - 1;
        @NotNull final Task task = taskRepository.findAll().get(0);
        taskRepository.removeOne(task);
        Assert.assertEquals(expectedNumberOfEntries, taskRepository.getSize());
    }

    @Test
    public void testRemoveForUser() {
        final int expectedNumberOfEntries = taskRepository.getSize(USER_ID_TWO) - 1;
        @NotNull final Task task = taskRepository.findAll(USER_ID_TWO).get(0);
        taskRepository.removeOne(USER_ID_TWO, task);
        Assert.assertEquals(expectedNumberOfEntries, taskRepository.getSize(USER_ID_TWO));
    }

    @Test
    public void testRemoveById() {
        final int expectedNumberOfEntries = taskRepository.getSize() - 1;
        @NotNull final String taskId = taskRepository.findAll().get(0).getId();
        taskRepository.removeOneById(taskId);
        Assert.assertEquals(expectedNumberOfEntries, taskRepository.getSize());
    }

    @Test
    public void testRemoveByIdForUser() {
        final int expectedNumberOfEntries = taskRepository.getSize(USER_ID_ONE) - 1;
        @NotNull final String taskId = taskRepository.findAll(USER_ID_ONE).get(0).getId();
        taskRepository.removeOneById(USER_ID_ONE, taskId);
        Assert.assertEquals(expectedNumberOfEntries, taskRepository.getSize(USER_ID_ONE));
    }

    @Test
    public void testRemoveByIndex() {
        final int expectedNumberOfEntries = taskRepository.getSize() - 1;
        taskRepository.removeOneByIndex(1);
        Assert.assertEquals(expectedNumberOfEntries, taskRepository.getSize());
    }

    @Test
    public void testRemoveByIndexTaskNotFound() {
        taskRepository.removeAll();
        Assert.assertNull(taskRepository.removeOneByIndex(1));
    }

    @Test
    public void testRemoveByIndexForUser() {
        final int expectedNumberOfEntries = taskRepository.getSize(USER_ID_TWO) - 1;
        taskRepository.removeOneByIndex(USER_ID_TWO, 1);
        Assert.assertEquals(expectedNumberOfEntries, taskRepository.getSize(USER_ID_TWO));
    }

    @Test
    public void testRemoveByIndexForUserTaskNotFound() {
        taskRepository.removeAll();
        Assert.assertNull(taskRepository.removeOneByIndex(USER_ID_ONE, 1));
    }

    @Test
    public void testFindAllByProjectId() {
        @NotNull final List<Task> tasksOne = taskRepository.findAll(USER_ID_ONE);
        @NotNull final List<Task> tasks = taskRepository.findAllByProjectId(USER_ID_ONE, projectOne.getId());
        Assert.assertEquals(tasksOne, tasks);
    }

    @Test
    public void testFindAllByIncorrectProjectId() {
        @NotNull final List<Task> tasks = taskRepository.findAllByProjectId(USER_ID_ONE, projectTwo.getId());
        Assert.assertEquals(Collections.emptyList(), tasks);
    }

    @After
    @SneakyThrows
    public void clearRepository() {
        taskRepository.set(tasks);
        projectRepository.removeOne(projectOne);
        projectRepository.removeOne(projectTwo);
        userRepository.removeOneById(USER_ID_ONE);
        userRepository.removeOneById(USER_ID_TWO);
        connection.close();
    }

}
