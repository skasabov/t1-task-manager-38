package ru.t1.skasabov.tm.taskmanager.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.skasabov.tm.client.AuthEndpoint;
import ru.t1.skasabov.tm.client.ProjectEndpoint;
import ru.t1.skasabov.tm.client.TaskEndpoint;
import ru.t1.skasabov.tm.client.UserEndpoint;
import ru.t1.skasabov.tm.dto.request.*;
import ru.t1.skasabov.tm.dto.response.*;
import ru.t1.skasabov.tm.enumerated.Sort;
import ru.t1.skasabov.tm.enumerated.Status;
import ru.t1.skasabov.tm.exception.field.SortIncorrectException;
import ru.t1.skasabov.tm.exception.field.StatusEmptyException;
import ru.t1.skasabov.tm.exception.field.StatusIncorrectException;
import ru.t1.skasabov.tm.model.Project;
import ru.t1.skasabov.tm.model.Task;
import ru.t1.skasabov.tm.taskmanager.marker.SoapCategory;

import javax.xml.ws.WebServiceException;
import java.util.List;

public class TaskEndpointTest {

    @NotNull
    private final AuthEndpoint authEndpoint = new AuthEndpoint();

    @NotNull
    private final UserEndpoint userEndpoint = new UserEndpoint();

    @NotNull
    private final ProjectEndpoint projectEndpoint = new ProjectEndpoint();

    @NotNull
    private final TaskEndpoint taskEndpoint = new TaskEndpoint();

    @Nullable
    private String token;

    @Nullable
    private String adminToken;

    @NotNull
    private List<Task> taskList;

    @Nullable
    private Project projectOne;

    @Nullable
    private Project projectTwo;

    @Nullable
    private Task taskOne;

    @Nullable
    private Task taskTwo;

    @Before
    public void initTest() {
        userEndpoint.registryUser(
                new UserRegistryRequest("123", "123", "123@123")
        );
        @NotNull final UserLoginResponse response = authEndpoint.login(
                new UserLoginRequest("123", "123")
        );
        token = response.getToken();
        @NotNull final UserLoginResponse adminResponse = authEndpoint.login(
                new UserLoginRequest("admin", "admin")
        );
        adminToken = adminResponse.getToken();
        @NotNull final ProjectCreateRequest requestProjectOne = new ProjectCreateRequest(
                "project_one", "project_one"
        );
        requestProjectOne.setToken(token);
        @NotNull final ProjectCreateRequest requestProjectTwo = new ProjectCreateRequest(
                "project_two", "project_two"
        );
        requestProjectTwo.setToken(token);
        @NotNull final ProjectCreateResponse responseProjectTwo = projectEndpoint.createProject(requestProjectTwo);
        @NotNull final ProjectCreateResponse responseProjectOne = projectEndpoint.createProject(requestProjectOne);
        projectOne = responseProjectOne.getProject();
        projectTwo = responseProjectTwo.getProject();
        @NotNull final TaskCreateRequest requestTaskOne = new TaskCreateRequest("task_one", "task_one");
        requestTaskOne.setToken(token);
        @NotNull final TaskCreateRequest requestTaskTwo = new TaskCreateRequest("task_two", "task_two");
        requestTaskTwo.setToken(token);
        @NotNull final TaskCreateResponse responseTaskTwo = taskEndpoint.createTask(requestTaskTwo);
        @NotNull final TaskCreateResponse responseTaskOne = taskEndpoint.createTask(requestTaskOne);
        Assert.assertNotNull(responseProjectOne.getProject());
        Assert.assertNotNull(responseProjectTwo.getProject());
        Assert.assertNotNull(responseTaskOne.getTask());
        Assert.assertNotNull(responseTaskTwo.getTask());
        @NotNull final TaskBindToProjectRequest requestBindTaskOne = new TaskBindToProjectRequest(
                responseProjectOne.getProject().getId(), responseTaskOne.getTask().getId()
        );
        @NotNull final TaskBindToProjectRequest requestBindTaskTwo = new TaskBindToProjectRequest(
                responseProjectTwo.getProject().getId(), responseTaskTwo.getTask().getId()
        );
        requestBindTaskOne.setToken(token);
        requestBindTaskTwo.setToken(token);
        @NotNull final TaskBindToProjectResponse responseBindTaskOne = taskEndpoint.bindTaskToProject(requestBindTaskOne);
        @NotNull final TaskBindToProjectResponse responseBindTaskTwo = taskEndpoint.bindTaskToProject(requestBindTaskTwo);
        taskOne = responseBindTaskOne.getTask();
        taskTwo = responseBindTaskTwo.getTask();
        @NotNull final TaskListRequest taskListRequest = new TaskListRequest();
        taskListRequest.setToken(token);
        taskList = taskEndpoint.listTasks(taskListRequest).getTasks();
    }

    @Test
    @Category(SoapCategory.class)
    public void testFindAllByProjectId() {
        Assert.assertNotNull(projectOne);
        @NotNull final TaskGetByProjectIdRequest request = new TaskGetByProjectIdRequest(projectOne.getId());
        request.setToken(token);
        @NotNull final TaskGetByProjectIdResponse response = taskEndpoint.findAllByProjectId(request);
        Assert.assertEquals(1, response.getTasks().size());
    }

    @Test
    @Category(SoapCategory.class)
    public void testFindAllByEmptyProjectId() {
        @NotNull final TaskGetByProjectIdRequest request = new TaskGetByProjectIdRequest("");
        request.setToken(token);
        @NotNull final TaskGetByProjectIdResponse response = taskEndpoint.findAllByProjectId(request);
        Assert.assertEquals(0, response.getTasks().size());
    }

    @Test
    @Category(SoapCategory.class)
    public void testFindAllByInvalidProjectId() {
        @NotNull final TaskGetByProjectIdRequest request = new TaskGetByProjectIdRequest("???");
        request.setToken(token);
        @NotNull final TaskGetByProjectIdResponse response = taskEndpoint.findAllByProjectId(request);
        Assert.assertEquals(0, response.getTasks().size());
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testFindAllByProjectIdNoAuth() {
        Assert.assertNotNull(projectOne);
        @NotNull final TaskGetByProjectIdRequest request = new TaskGetByProjectIdRequest(projectOne.getId());
        taskEndpoint.findAllByProjectId(request);
    }

    @Test
    @Category(SoapCategory.class)
    public void testBindTaskToProject() {
        Assert.assertNotNull(projectTwo);
        Assert.assertNotNull(taskOne);
        @NotNull final TaskBindToProjectRequest request = new TaskBindToProjectRequest(
                projectTwo.getId(), taskOne.getId()
        );
        request.setToken(token);
        @NotNull final TaskBindToProjectResponse response = taskEndpoint.bindTaskToProject(request);
        Assert.assertNotNull(response.getTask());
        Assert.assertEquals(projectTwo.getId(), response.getTask().getProjectId());
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testBindTaskToEmptyProject() {
        Assert.assertNotNull(taskOne);
        @NotNull final TaskBindToProjectRequest request = new TaskBindToProjectRequest(
                "", taskOne.getId()
        );
        request.setToken(token);
        taskEndpoint.bindTaskToProject(request);
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testBindEmptyTaskToProject() {
        Assert.assertNotNull(projectTwo);
        @NotNull final TaskBindToProjectRequest request = new TaskBindToProjectRequest(
                projectTwo.getId(), ""
        );
        request.setToken(token);
        taskEndpoint.bindTaskToProject(request);
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testBindTaskToInvalidProject() {
        Assert.assertNotNull(taskOne);
        @NotNull final TaskBindToProjectRequest request = new TaskBindToProjectRequest(
                "???", taskOne.getId()
        );
        request.setToken(token);
        taskEndpoint.bindTaskToProject(request);
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testBindInvalidTaskToProject() {
        Assert.assertNotNull(projectTwo);
        @NotNull final TaskBindToProjectRequest request = new TaskBindToProjectRequest(
                projectTwo.getId(), "???"
        );
        request.setToken(token);
        taskEndpoint.bindTaskToProject(request);
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testBindTaskToProjectNoAuth() {
        Assert.assertNotNull(taskOne);
        Assert.assertNotNull(projectTwo);
        @NotNull final TaskBindToProjectRequest request = new TaskBindToProjectRequest(
                projectTwo.getId(), taskOne.getId()
        );
        taskEndpoint.bindTaskToProject(request);
    }

    @Test
    @Category(SoapCategory.class)
    public void testUnBindTaskFromProject() {
        Assert.assertNotNull(taskOne);
        Assert.assertNotNull(projectOne);
        @NotNull final TaskUnbindFromProjectRequest request = new TaskUnbindFromProjectRequest(
                projectOne.getId(), taskOne.getId()
        );
        request.setToken(token);
        @NotNull final TaskUnbindFromProjectResponse response = taskEndpoint.unbindTaskFromProject(request);
        Assert.assertNotNull(response.getTask());
        Assert.assertNull(response.getTask().getProjectId());
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testUnBindTaskFromEmptyProject() {
        Assert.assertNotNull(taskOne);
        @NotNull final TaskUnbindFromProjectRequest request = new TaskUnbindFromProjectRequest(
                "", taskOne.getId()
        );
        request.setToken(token);
        taskEndpoint.unbindTaskFromProject(request);
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testUnBindEmptyTaskFromProject() {
        Assert.assertNotNull(projectOne);
        @NotNull final TaskUnbindFromProjectRequest request = new TaskUnbindFromProjectRequest(
                projectOne.getId(), ""
        );
        request.setToken(token);
        taskEndpoint.unbindTaskFromProject(request);
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testUnBindTaskFromInvalidProject() {
        Assert.assertNotNull(taskOne);
        @NotNull final TaskUnbindFromProjectRequest request = new TaskUnbindFromProjectRequest(
                "???", taskOne.getId()
        );
        request.setToken(token);
        taskEndpoint.unbindTaskFromProject(request);
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testUnBindInvalidTaskFromProject() {
        Assert.assertNotNull(projectOne);
        @NotNull final TaskUnbindFromProjectRequest request = new TaskUnbindFromProjectRequest(
                projectOne.getId(), "???"
        );
        request.setToken(token);
        taskEndpoint.unbindTaskFromProject(request);
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testUnBindTaskFromProjectNoAuth() {
        Assert.assertNotNull(taskOne);
        Assert.assertNotNull(projectOne);
        @NotNull final TaskUnbindFromProjectRequest request = new TaskUnbindFromProjectRequest(
                projectOne.getId(), taskOne.getId()
        );
        taskEndpoint.unbindTaskFromProject(request);
    }

    @Test
    @Category(SoapCategory.class)
    public void testChangeTaskStatusById() {
        Assert.assertNotNull(taskOne);
        @NotNull final TaskChangeStatusByIdRequest request = new TaskChangeStatusByIdRequest(
                taskOne.getId(), Status.NOT_STARTED
        );
        request.setToken(token);
        @NotNull final TaskChangeStatusByIdResponse response = taskEndpoint.changeTaskStatusById(request);
        Assert.assertNotNull(response.getTask());
        Assert.assertEquals(Status.NOT_STARTED, response.getTask().getStatus());
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testChangeTaskStatusByEmptyId() {
        @NotNull final TaskChangeStatusByIdRequest request = new TaskChangeStatusByIdRequest(
                "", Status.NOT_STARTED
        );
        request.setToken(token);
        taskEndpoint.changeTaskStatusById(request);
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testChangeTaskStatusByInvalidId() {
        @NotNull final TaskChangeStatusByIdRequest request = new TaskChangeStatusByIdRequest(
                "???", Status.NOT_STARTED
        );
        request.setToken(token);
        taskEndpoint.changeTaskStatusById(request);
    }

    @Test(expected = StatusEmptyException.class)
    @Category(SoapCategory.class)
    public void testChangeTaskEmptyStatusById() {
        Assert.assertNotNull(taskOne);
        @NotNull final TaskChangeStatusByIdRequest request = new TaskChangeStatusByIdRequest(
                taskOne.getId(), Status.toStatus(null)
        );
        request.setToken(token);
        taskEndpoint.changeTaskStatusById(request);
    }

    @Test(expected = StatusIncorrectException.class)
    @Category(SoapCategory.class)
    public void testChangeTaskInvalidStatusById() {
        Assert.assertNotNull(taskOne);
        new TaskChangeStatusByIdRequest(taskOne.getId(), Status.toStatus("???"));
    }

    @Test
    @Category(SoapCategory.class)
    public void testChangeTaskStatusByIndex() {
        @NotNull final TaskChangeStatusByIndexRequest request = new TaskChangeStatusByIndexRequest(
                1, Status.COMPLETED
        );
        request.setToken(token);
        @NotNull final TaskChangeStatusByIndexResponse response = taskEndpoint.changeTaskStatusByIndex(request);
        Assert.assertNotNull(response.getTask());
        Assert.assertEquals(Status.COMPLETED, response.getTask().getStatus());
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testChangeTaskStatusByEmptyIndex() {
        @NotNull final TaskChangeStatusByIndexRequest request = new TaskChangeStatusByIndexRequest(
                null, Status.NOT_STARTED
        );
        request.setToken(token);
        taskEndpoint.changeTaskStatusByIndex(request);
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testChangeTaskStatusByNegativeIndex() {
        @NotNull final TaskChangeStatusByIndexRequest request = new TaskChangeStatusByIndexRequest(
                -2, Status.NOT_STARTED
        );
        request.setToken(token);
        taskEndpoint.changeTaskStatusByIndex(request);
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testChangeTaskStatusByInvalidIndex() {
        @NotNull final TaskChangeStatusByIndexRequest request = new TaskChangeStatusByIndexRequest(
                3, Status.NOT_STARTED
        );
        request.setToken(token);
        taskEndpoint.changeTaskStatusByIndex(request);
    }

    @Test(expected = StatusEmptyException.class)
    @Category(SoapCategory.class)
    public void testChangeTaskEmptyStatusByIndex() {
        @NotNull final TaskChangeStatusByIndexRequest request = new TaskChangeStatusByIndexRequest(
                1, Status.toStatus(null)
        );
        request.setToken(token);
        taskEndpoint.changeTaskStatusByIndex(request);
    }

    @Test(expected = StatusIncorrectException.class)
    @Category(SoapCategory.class)
    public void testChangeTaskInvalidStatusByIndex() {
        new TaskChangeStatusByIndexRequest(1, Status.toStatus("???"));
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testChangeTaskStatusByIndexNoAuth() {
        @NotNull final TaskChangeStatusByIndexRequest request = new TaskChangeStatusByIndexRequest(
                1, Status.NOT_STARTED
        );
        taskEndpoint.changeTaskStatusByIndex(request);
    }

    @Test
    @Category(SoapCategory.class)
    public void testClearTasks() {
        @NotNull final TaskClearRequest request = new TaskClearRequest();
        request.setToken(token);
        @NotNull final TaskClearResponse response = taskEndpoint.clearTasks(request);
        Assert.assertEquals(0, response.getTasks().size());
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testClearTasksNoAuth() {
        @NotNull final TaskClearRequest request = new TaskClearRequest();
        taskEndpoint.clearTasks(request);
    }

    @Test
    @Category(SoapCategory.class)
    public void testCreateTask() {
        @NotNull final TaskCreateRequest request = new TaskCreateRequest("task_three", "task_three");
        request.setToken(token);
        @NotNull final TaskCreateResponse response = taskEndpoint.createTask(request);
        Assert.assertNotNull(response.getTask());
        Assert.assertEquals("task_three", response.getTask().getName());
        Assert.assertEquals("task_three", response.getTask().getDescription());
    }

    @Test
    @Category(SoapCategory.class)
    public void testCreateTaskWithName() {
        @NotNull final TaskCreateRequest request = new TaskCreateRequest("task_three", "");
        request.setToken(token);
        @NotNull final TaskCreateResponse response = taskEndpoint.createTask(request);
        Assert.assertNotNull(response.getTask());
        Assert.assertEquals("task_three", response.getTask().getName());
        Assert.assertEquals("", response.getTask().getDescription());
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testCreateTaskWithEmptyName() {
        @NotNull final TaskCreateRequest request = new TaskCreateRequest("", "task_three");
        request.setToken(token);
        taskEndpoint.createTask(request);
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testCreateTaskNoAuth() {
        @NotNull final TaskCreateRequest request = new TaskCreateRequest("task_three", "task_three");
        taskEndpoint.createTask(request);
    }

    @Test
    @Category(SoapCategory.class)
    public void testListTasks() {
        @NotNull final TaskListRequest request = new TaskListRequest();
        request.setToken(token);
        @NotNull final TaskListResponse response = taskEndpoint.listTasks(request);
        Assert.assertEquals(2, response.getTasks().size());
    }

    @Test
    @Category(SoapCategory.class)
    public void testListTasksWithSort() {
        @NotNull final TaskListRequest request = new TaskListRequest(Sort.BY_NAME);
        request.setToken(token);
        @NotNull final TaskListResponse response = taskEndpoint.listTasks(request);
        Assert.assertEquals("task_one", response.getTasks().get(0).getName());
        Assert.assertEquals("task_two", response.getTasks().get(1).getName());
    }

    @Test(expected = SortIncorrectException.class)
    @Category(SoapCategory.class)
    public void testListTasksWithInvalidSort() {
        new TaskListRequest(Sort.toSort("???"));
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testListTasksNoAuth() {
        @NotNull final TaskListRequest request = new TaskListRequest();
        taskEndpoint.listTasks(request);
    }

    @Test
    @Category(SoapCategory.class)
    public void testGetTaskById() {
        Assert.assertNotNull(taskOne);
        @NotNull final TaskGetByIdRequest request = new TaskGetByIdRequest(taskOne.getId());
        request.setToken(token);
        @NotNull final TaskGetByIdResponse response = taskEndpoint.getTaskById(request);
        Assert.assertNotNull(response.getTask());
        Assert.assertEquals(taskOne.getName(), response.getTask().getName());
        Assert.assertEquals(taskOne.getDescription(), response.getTask().getDescription());
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testGetTaskByEmptyId() {
        @NotNull final TaskGetByIdRequest request = new TaskGetByIdRequest("");
        request.setToken(token);
        taskEndpoint.getTaskById(request);
    }

    @Test
    @Category(SoapCategory.class)
    public void testGetTaskByInvalidId() {
        @NotNull final TaskGetByIdRequest request = new TaskGetByIdRequest("???");
        request.setToken(token);
        @NotNull final TaskGetByIdResponse response = taskEndpoint.getTaskById(request);
        Assert.assertNull(response.getTask());
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testGetTaskByIdNoAuth() {
        Assert.assertNotNull(taskOne);
        @NotNull final TaskGetByIdRequest request = new TaskGetByIdRequest(taskOne.getId());
        taskEndpoint.getTaskById(request);
    }

    @Test
    @Category(SoapCategory.class)
    public void testGetTaskByIndex() {
        @NotNull final Task task = taskList.get(0);
        @NotNull final TaskGetByIndexRequest request = new TaskGetByIndexRequest(1);
        request.setToken(token);
        @NotNull final TaskGetByIndexResponse response = taskEndpoint.getTaskByIndex(request);
        Assert.assertNotNull(response.getTask());
        Assert.assertEquals(task.getName(), response.getTask().getName());
        Assert.assertEquals(task.getDescription(), response.getTask().getDescription());
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testGetTaskByEmptyIndex() {
        @NotNull final TaskGetByIndexRequest request = new TaskGetByIndexRequest(null);
        request.setToken(token);
        taskEndpoint.getTaskByIndex(request);
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testGetTaskByNegativeIndex() {
        @NotNull final TaskGetByIndexRequest request = new TaskGetByIndexRequest(-2);
        request.setToken(token);
        taskEndpoint.getTaskByIndex(request);
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testGetTaskByInvalidIndex() {
        @NotNull final TaskGetByIndexRequest request = new TaskGetByIndexRequest(3);
        request.setToken(token);
        taskEndpoint.getTaskByIndex(request);
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testGetTaskByIndexNoAuth() {
        @NotNull final TaskGetByIndexRequest request = new TaskGetByIndexRequest(1);
        taskEndpoint.getTaskByIndex(request);
    }

    @Test
    @Category(SoapCategory.class)
    public void testRemoveTaskById() {
        Assert.assertNotNull(taskOne);
        @NotNull final TaskRemoveByIdRequest request = new TaskRemoveByIdRequest(taskOne.getId());
        request.setToken(token);
        @NotNull final TaskRemoveByIdResponse response = taskEndpoint.removeTaskById(request);
        Assert.assertNotNull(response.getTask());
        Assert.assertEquals(taskOne.getName(), response.getTask().getName());
        Assert.assertEquals(taskOne.getDescription(), response.getTask().getDescription());
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testRemoveTaskByEmptyId() {
        @NotNull final TaskRemoveByIdRequest request = new TaskRemoveByIdRequest("");
        request.setToken(token);
        taskEndpoint.removeTaskById(request);
    }

    @Test
    @Category(SoapCategory.class)
    public void testRemoveTaskByInvalidId() {
        @NotNull final TaskRemoveByIdRequest request = new TaskRemoveByIdRequest("???");
        request.setToken(token);
        @NotNull final TaskRemoveByIdResponse response = taskEndpoint.removeTaskById(request);
        Assert.assertNull(response.getTask());
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testRemoveTaskByIdNoAuth() {
        Assert.assertNotNull(taskOne);
        @NotNull final TaskRemoveByIdRequest request = new TaskRemoveByIdRequest(taskOne.getId());
        taskEndpoint.removeTaskById(request);
    }

    @Test
    @Category(SoapCategory.class)
    public void testRemoveTaskByIndex() {
        @NotNull final Task task = taskList.get(0);
        @NotNull final TaskRemoveByIndexRequest request = new TaskRemoveByIndexRequest(1);
        request.setToken(token);
        @NotNull final TaskRemoveByIndexResponse response = taskEndpoint.removeTaskByIndex(request);
        Assert.assertNotNull(response.getTask());
        Assert.assertEquals(task.getName(), response.getTask().getName());
        Assert.assertEquals(task.getDescription(), response.getTask().getDescription());
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testRemoveTaskByEmptyIndex() {
        @NotNull final TaskRemoveByIndexRequest request = new TaskRemoveByIndexRequest(null);
        request.setToken(token);
        taskEndpoint.removeTaskByIndex(request);
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testRemoveTaskByNegativeIndex() {
        @NotNull final TaskRemoveByIndexRequest request = new TaskRemoveByIndexRequest(-2);
        request.setToken(token);
        taskEndpoint.removeTaskByIndex(request);
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testRemoveTaskByInvalidIndex() {
        @NotNull final TaskRemoveByIndexRequest request = new TaskRemoveByIndexRequest(3);
        request.setToken(token);
        taskEndpoint.removeTaskByIndex(request);
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testRemoveTaskByIndexNoAuth() {
        @NotNull final TaskRemoveByIndexRequest request = new TaskRemoveByIndexRequest(1);
        taskEndpoint.removeTaskByIndex(request);
    }

    @Test
    @Category(SoapCategory.class)
    public void testUpdateTaskById() {
        Assert.assertNotNull(taskOne);
        @NotNull final TaskUpdateByIdRequest request = new TaskUpdateByIdRequest(
                taskOne.getId(), "task_three", "task_three"
        );
        request.setToken(token);
        @NotNull final TaskUpdateByIdResponse response = taskEndpoint.updateTaskById(request);
        Assert.assertNotNull(response.getTask());
        Assert.assertEquals("task_three", response.getTask().getName());
        Assert.assertEquals("task_three", response.getTask().getDescription());
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testUpdateTaskByEmptyId() {
        @NotNull final TaskUpdateByIdRequest request = new TaskUpdateByIdRequest(
                "", "task_three", "task_three"
        );
        request.setToken(token);
        taskEndpoint.updateTaskById(request);
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testUpdateTaskByInvalidId() {
        @NotNull final TaskUpdateByIdRequest request = new TaskUpdateByIdRequest(
                "???", "task_three", "task_three"
        );
        request.setToken(token);
        taskEndpoint.updateTaskById(request);
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testUpdateTaskByIdWithEmptyName() {
        Assert.assertNotNull(taskOne);
        @NotNull final TaskUpdateByIdRequest request = new TaskUpdateByIdRequest(
                taskOne.getId(), "", "task_three"
        );
        request.setToken(token);
        taskEndpoint.updateTaskById(request);
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testUpdateTaskByIdNoAuth() {
        Assert.assertNotNull(taskOne);
        @NotNull final TaskUpdateByIdRequest request = new TaskUpdateByIdRequest(
                taskOne.getId(), "task_three", "task_three"
        );
        taskEndpoint.updateTaskById(request);
    }

    @Test
    @Category(SoapCategory.class)
    public void testUpdateTaskByIndex() {
        @NotNull final TaskUpdateByIndexRequest request = new TaskUpdateByIndexRequest(
                1, "task_three", "task_three"
        );
        request.setToken(token);
        @NotNull final TaskUpdateByIndexResponse response = taskEndpoint.updateTaskByIndex(request);
        Assert.assertNotNull(response.getTask());
        Assert.assertEquals("task_three", response.getTask().getName());
        Assert.assertEquals("task_three", response.getTask().getDescription());
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testUpdateTaskByEmptyIndex() {
        @NotNull final TaskUpdateByIndexRequest request = new TaskUpdateByIndexRequest(
                null, "task_three", "task_three"
        );
        request.setToken(token);
        taskEndpoint.updateTaskByIndex(request);
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testUpdateTaskByNegativeIndex() {
        @NotNull final TaskUpdateByIndexRequest request = new TaskUpdateByIndexRequest(
                -2, "task_three", "task_three"
        );
        request.setToken(token);
        taskEndpoint.updateTaskByIndex(request);
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testUpdateTaskByInvalidIndex() {
        @NotNull final TaskUpdateByIndexRequest request = new TaskUpdateByIndexRequest(
                3, "task_three", "task_three"
        );
        request.setToken(token);
        taskEndpoint.updateTaskByIndex(request);
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testUpdateTaskByIndexWithName() {
        @NotNull final TaskUpdateByIndexRequest request = new TaskUpdateByIndexRequest(
                1, "", "task_three"
        );
        request.setToken(token);
        taskEndpoint.updateTaskByIndex(request);
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testUpdateTaskByIndexNoAuth() {
        @NotNull final TaskUpdateByIndexRequest request = new TaskUpdateByIndexRequest(
                1, "task_three", "task_three"
        );
        taskEndpoint.updateTaskByIndex(request);
    }

    @Test
    @Category(SoapCategory.class)
    public void testStartTaskById() {
        Assert.assertNotNull(taskOne);
        @NotNull final TaskStartByIdRequest request = new TaskStartByIdRequest(taskOne.getId());
        request.setToken(token);
        @NotNull final TaskStartByIdResponse response = taskEndpoint.startTaskById(request);
        Assert.assertNotNull(response.getTask());
        Assert.assertEquals(Status.IN_PROGRESS, response.getTask().getStatus());
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testStartTaskByEmptyId() {
        @NotNull final TaskStartByIdRequest request = new TaskStartByIdRequest("");
        request.setToken(token);
        taskEndpoint.startTaskById(request);
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testStartTaskByInvalidId() {
        @NotNull final TaskStartByIdRequest request = new TaskStartByIdRequest("???");
        request.setToken(token);
        taskEndpoint.startTaskById(request);
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testStartTaskByIdNoAuth() {
        Assert.assertNotNull(taskOne);
        @NotNull final TaskStartByIdRequest request = new TaskStartByIdRequest(taskOne.getId());
        taskEndpoint.startTaskById(request);
    }

    @Test
    @Category(SoapCategory.class)
    public void testStartTaskByIndex() {
        @NotNull final TaskStartByIndexRequest request = new TaskStartByIndexRequest(1);
        request.setToken(token);
        @NotNull final TaskStartByIndexResponse response = taskEndpoint.startTaskByIndex(request);
        Assert.assertNotNull(response.getTask());
        Assert.assertEquals(Status.IN_PROGRESS, response.getTask().getStatus());
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testStartTaskByEmptyIndex() {
        @NotNull final TaskStartByIndexRequest request = new TaskStartByIndexRequest(null);
        request.setToken(token);
        taskEndpoint.startTaskByIndex(request);
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testStartTaskByNegativeIndex() {
        @NotNull final TaskStartByIndexRequest request = new TaskStartByIndexRequest(-2);
        request.setToken(token);
        taskEndpoint.startTaskByIndex(request);
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testStartTaskByInvalidIndex() {
        @NotNull final TaskStartByIndexRequest request = new TaskStartByIndexRequest(3);
        request.setToken(token);
        taskEndpoint.startTaskByIndex(request);
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testStartTaskByIndexNoAuth() {
        @NotNull final TaskStartByIndexRequest request = new TaskStartByIndexRequest(1);
        taskEndpoint.startTaskByIndex(request);
    }

    @Test
    @Category(SoapCategory.class)
    public void testCompleteTaskById() {
        Assert.assertNotNull(taskOne);
        @NotNull final TaskCompleteByIdRequest request = new TaskCompleteByIdRequest(taskOne.getId());
        request.setToken(token);
        @NotNull final TaskCompleteByIdResponse response = taskEndpoint.completeTaskById(request);
        Assert.assertNotNull(response.getTask());
        Assert.assertEquals(Status.COMPLETED, response.getTask().getStatus());
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testCompleteTaskByEmptyId() {
        @NotNull final TaskCompleteByIdRequest request = new TaskCompleteByIdRequest("");
        request.setToken(token);
        taskEndpoint.completeTaskById(request);
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testCompleteTaskByInvalidId() {
        @NotNull final TaskCompleteByIdRequest request = new TaskCompleteByIdRequest("???");
        request.setToken(token);
        taskEndpoint.completeTaskById(request);
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testCompleteTaskByIdNoAuth() {
        Assert.assertNotNull(taskOne);
        @NotNull final TaskCompleteByIdRequest request = new TaskCompleteByIdRequest(taskOne.getId());
        taskEndpoint.completeTaskById(request);
    }

    @Test
    @Category(SoapCategory.class)
    public void testCompleteTaskByIndex() {
        @NotNull final TaskCompleteByIndexRequest request = new TaskCompleteByIndexRequest(1);
        request.setToken(token);
        @NotNull final TaskCompleteByIndexResponse response = taskEndpoint.completeTaskByIndex(request);
        Assert.assertNotNull(response.getTask());
        Assert.assertEquals(Status.COMPLETED, response.getTask().getStatus());
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testCompleteTaskByEmptyIndex() {
        @NotNull final TaskCompleteByIndexRequest request = new TaskCompleteByIndexRequest(null);
        request.setToken(token);
        taskEndpoint.completeTaskByIndex(request);
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testCompleteTaskByNegativeIndex() {
        @NotNull final TaskCompleteByIndexRequest request = new TaskCompleteByIndexRequest(-2);
        request.setToken(token);
        taskEndpoint.completeTaskByIndex(request);
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testCompleteTaskByInvalidIndex() {
        @NotNull final TaskCompleteByIndexRequest request = new TaskCompleteByIndexRequest(3);
        request.setToken(token);
        taskEndpoint.completeTaskByIndex(request);
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testCompleteTaskByIndexNoAuth() {
        @NotNull final TaskCompleteByIndexRequest request = new TaskCompleteByIndexRequest(1);
        taskEndpoint.completeTaskByIndex(request);
    }

    @After
    public void endTest() {
        Assert.assertNotNull(taskOne);
        Assert.assertNotNull(taskTwo);
        Assert.assertNotNull(projectOne);
        Assert.assertNotNull(projectTwo);
        @NotNull final TaskRemoveByIdRequest taskRequestOne = new TaskRemoveByIdRequest(
                taskOne.getId()
        );
        @NotNull final TaskRemoveByIdRequest taskRequestTwo = new TaskRemoveByIdRequest(
                taskTwo.getId()
        );
        @NotNull final ProjectRemoveByIdRequest projectRequestOne = new ProjectRemoveByIdRequest(
                projectOne.getId()
        );
        @NotNull final ProjectRemoveByIdRequest projectRequestTwo = new ProjectRemoveByIdRequest(
                projectTwo.getId()
        );
        @NotNull final UserLogoutRequest userLogoutRequest = new UserLogoutRequest();
        taskRequestOne.setToken(token);
        taskRequestTwo.setToken(token);
        projectRequestOne.setToken(token);
        projectRequestTwo.setToken(token);
        userLogoutRequest.setToken(token);
        taskEndpoint.removeTaskById(taskRequestOne);
        taskEndpoint.removeTaskById(taskRequestTwo);
        projectEndpoint.removeProjectById(projectRequestOne);
        projectEndpoint.removeProjectById(projectRequestTwo);
        authEndpoint.logout(userLogoutRequest);
        @NotNull final UserRemoveRequest removeRequest = new UserRemoveRequest("123");
        removeRequest.setToken(adminToken);
        userEndpoint.removeUser(removeRequest);
        @NotNull final UserLogoutRequest adminLogoutRequest = new UserLogoutRequest();
        adminLogoutRequest.setToken(adminToken);
        authEndpoint.logout(adminLogoutRequest);
    }

}
