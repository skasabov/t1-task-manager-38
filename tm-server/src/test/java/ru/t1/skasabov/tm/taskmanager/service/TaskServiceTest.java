package ru.t1.skasabov.tm.taskmanager.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.t1.skasabov.tm.api.service.*;
import ru.t1.skasabov.tm.enumerated.Status;
import ru.t1.skasabov.tm.enumerated.TaskSort;
import ru.t1.skasabov.tm.exception.entity.ModelEmptyException;
import ru.t1.skasabov.tm.exception.entity.TaskNotFoundException;
import ru.t1.skasabov.tm.exception.field.*;
import ru.t1.skasabov.tm.model.Project;
import ru.t1.skasabov.tm.model.Task;
import ru.t1.skasabov.tm.service.*;

import java.util.*;

public class TaskServiceTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    @NotNull
    private static String USER_ID_ONE;

    @NotNull
    private static String USER_ID_TWO;

    @NotNull
    private Project projectOne;

    @NotNull
    private Project projectTwo;

    @NotNull
    private List<Task> tasks;

    @NotNull
    private ITaskService taskService;

    @NotNull
    private IProjectService projectService;

    @NotNull
    private IUserService userService;

    @Before
    public void initTest() {
        @NotNull final IPropertyService propertyService = new PropertyService();
        @NotNull final IConnectionService connectionService = new ConnectionService(propertyService);
        projectService = new ProjectService(connectionService);
        userService = new UserService(connectionService, propertyService);
        taskService = new TaskService(connectionService);
        tasks = taskService.findAll();
        taskService.removeAll();
        USER_ID_ONE = userService.create("user_one", "user_one").getId();
        USER_ID_TWO = userService.create("user_two", "user_two").getId();
        projectOne = projectService.create(USER_ID_ONE, "project_one");
        projectTwo = projectService.create(USER_ID_TWO, "project_two");
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final Task task = new Task();
            task.setName("Test " + i);
            if (i <= 5) {
                task.setUserId(USER_ID_ONE);
                task.setProjectId(projectOne.getId());
            } else {
                task.setUserId(USER_ID_TWO);
                task.setProjectId(projectTwo.getId());
            }
            taskService.add(task);
        }
    }

    @Test(expected = UserIdEmptyException.class)
    public void testCreateForEmptyUser() {
        taskService.create("", "test", "");
    }

    @Test(expected = NameEmptyException.class)
    public void testCreateEmptyName() {
        taskService.create(USER_ID_ONE, "", "");
    }

    @Test
    public void testCreateName() {
        final int expectedNumberOfEntries = taskService.getSize() + 1;
        @NotNull final String name = "Test Task";
        @NotNull final Task actualTask = taskService.create(USER_ID_ONE, name, "");
        Assert.assertEquals(expectedNumberOfEntries, taskService.getSize());
        Assert.assertEquals(USER_ID_ONE, actualTask.getUserId());
        Assert.assertEquals(name, actualTask.getName());
    }

    @Test
    public void testCreateDescription() {
        final int expectedNumberOfEntries = taskService.getSize() + 1;
        @NotNull final String name = "Test Task";
        @NotNull final String description = "Test Description";
        @NotNull final Task actualTask = taskService.create(USER_ID_TWO, name, description);
        Assert.assertEquals(expectedNumberOfEntries, taskService.getSize());
        Assert.assertEquals(USER_ID_TWO, actualTask.getUserId());
        Assert.assertEquals(name, actualTask.getName());
        Assert.assertEquals(description, actualTask.getDescription());
    }

    @Test
    public void createTask() {
        final int expectedNumberOfEntries = taskService.getSize() + 1;
        @NotNull final String name = "Test Task";
        @NotNull final String description = "Test Description";
        @NotNull final Date dateBegin = new Date();
        @NotNull final Date dateEnd = new Date();
        @NotNull final Task actualTask = taskService.create(USER_ID_ONE, name, description, dateBegin, dateEnd);
        Assert.assertEquals(expectedNumberOfEntries, taskService.getSize());
        Assert.assertEquals(USER_ID_ONE, actualTask.getUserId());
        Assert.assertEquals(name, actualTask.getName());
        Assert.assertEquals(description, actualTask.getDescription());
        Assert.assertEquals(dateBegin, actualTask.getDateBegin());
        Assert.assertEquals(dateEnd, actualTask.getDateEnd());
    }

    @Test
    public void testUpdateById() {
        @NotNull final String id = taskService.findAll(USER_ID_TWO).get(0).getId();
        @NotNull final String name = "Test Task One";
        @NotNull final String description = "Test Description One";
        @NotNull final Task task = taskService.updateById(USER_ID_TWO, id, name, description);
        Assert.assertEquals(USER_ID_TWO, task.getUserId());
        Assert.assertEquals(name, task.getName());
        Assert.assertEquals(description, task.getDescription());
    }

    @Test(expected = IdEmptyException.class)
    public void testUpdateByEmptyId() {
        @NotNull final String name = "Test Task One";
        @NotNull final String description = "Test Description One";
        taskService.updateById(USER_ID_TWO, "", name, description);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testUpdateByIdForEmptyUser() {
        @NotNull final String id = taskService.findAll(USER_ID_TWO).get(0).getId();
        @NotNull final String name = "Test Task One";
        @NotNull final String description = "Test Description One";
        taskService.updateById("", id, name, description);
    }

    @Test(expected = NameEmptyException.class)
    public void testUpdateByIdWithEmptyName() {
        @NotNull final String id = taskService.findAll(USER_ID_TWO).get(0).getId();
        @NotNull final String description = "Test Description One";
        taskService.updateById(USER_ID_TWO, id, "", description);
    }

    @Test(expected = TaskNotFoundException.class)
    public void testUpdateByIdTaskNotFound() {
        @NotNull final String id = UUID.randomUUID().toString();
        @NotNull final String name = "Test Task One";
        @NotNull final String description = "Test Description One";
        taskService.updateById(USER_ID_TWO, id, name, description);
    }

    @Test
    public void testUpdateByIndex() {
        @NotNull final String name = "Test Task One";
        @NotNull final String description = "Test Description One";
        @NotNull final Task task = taskService.updateByIndex(USER_ID_ONE, 1, name, description);
        Assert.assertEquals(USER_ID_ONE, task.getUserId());
        Assert.assertEquals(name, task.getName());
        Assert.assertEquals(description, task.getDescription());
    }

    @Test(expected = IndexIncorrectException.class)
    public void testUpdateByEmptyIndex() {
        @NotNull final String name = "Test Task One";
        @NotNull final String description = "Test Description One";
        taskService.updateByIndex(USER_ID_ONE, null, name, description);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testUpdateByIndexForEmptyUser() {
        @NotNull final String name = "Test Task One";
        @NotNull final String description = "Test Description One";
        taskService.updateByIndex("", 1, name, description);
    }

    @Test(expected = NameEmptyException.class)
    public void testUpdateByIndexWithEmptyName() {
        @NotNull final String description = "Test Description One";
        taskService.updateByIndex(USER_ID_ONE, 1, "", description);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testUpdateByNegativeIndex() {
        @NotNull final String name = "Test Task One";
        @NotNull final String description = "Test Description One";
        taskService.updateByIndex(USER_ID_ONE, -2, name, description);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testUpdateByIncorrectIndex() {
        @NotNull final String name = "Test Task One";
        @NotNull final String description = "Test Description One";
        taskService.updateByIndex(USER_ID_ONE, 5, name, description);
    }

    @Test
    public void testChangeTaskStatusById() {
        @NotNull final String id = taskService.findAll(USER_ID_TWO).get(0).getId();
        @NotNull final Status status = Status.NOT_STARTED;
        @NotNull final Task task = taskService.changeTaskStatusById(USER_ID_TWO, id, status);
        Assert.assertEquals(USER_ID_TWO, task.getUserId());
        Assert.assertEquals(status, task.getStatus());
    }

    @Test(expected = IdEmptyException.class)
    public void testChangeTaskStatusByEmptyId() {
        @NotNull final Status status = Status.NOT_STARTED;
        taskService.changeTaskStatusById(USER_ID_TWO, "", status);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testChangeTaskStatusByIdForEmptyUser() {
        @NotNull final String id = taskService.findAll(USER_ID_TWO).get(0).getId();
        @NotNull final Status status = Status.NOT_STARTED;
        taskService.changeTaskStatusById("", id, status);
    }

    @Test(expected = StatusEmptyException.class)
    public void testChangeTaskStatusByIdWithEmptyStatus() {
        @NotNull final String id = taskService.findAll(USER_ID_TWO).get(0).getId();
        taskService.changeTaskStatusById(USER_ID_TWO, id, Status.toStatus(null));
    }

    @Test(expected = StatusIncorrectException.class)
    public void testChangeTaskStatusByIdWithIncorrectStatus() {
        @NotNull final String id = taskService.findAll(USER_ID_TWO).get(0).getId();
        taskService.changeTaskStatusById(USER_ID_TWO, id, Status.toStatus("123"));
    }

    @Test(expected = TaskNotFoundException.class)
    public void testChangeTaskStatusByIdTaskNotFound() {
        @NotNull final String id = UUID.randomUUID().toString();
        @NotNull final Status status = Status.NOT_STARTED;
        taskService.changeTaskStatusById(USER_ID_TWO, id, status);
    }

    @Test
    public void testChangeTaskStatusByIndex() {
        @NotNull final Status status = Status.NOT_STARTED;
        @NotNull final Task task = taskService.changeTaskStatusByIndex(USER_ID_ONE, 1, status);
        Assert.assertEquals(USER_ID_ONE, task.getUserId());
        Assert.assertEquals(status, task.getStatus());
    }

    @Test(expected = IndexIncorrectException.class)
    public void testChangeTaskStatusByEmptyIndex() {
        @NotNull final Status status = Status.NOT_STARTED;
        taskService.changeTaskStatusByIndex(USER_ID_ONE, null, status);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testChangeTaskStatusByIndexForEmptyUser() {
        @NotNull final Status status = Status.NOT_STARTED;
        taskService.changeTaskStatusByIndex("", 1, status);
    }

    @Test(expected = StatusEmptyException.class)
    public void testChangeTaskStatusByIndexWithEmptyStatus() {
        taskService.changeTaskStatusByIndex(USER_ID_ONE, 1, Status.toStatus(null));
    }

    @Test(expected = StatusIncorrectException.class)
    public void testChangeTaskStatusByIndexWithIncorrectStatus() {
        taskService.changeTaskStatusByIndex(USER_ID_ONE, 1, Status.toStatus("123"));
    }

    @Test(expected = IndexIncorrectException.class)
    public void testChangeTaskStatusByIncorrectIndex() {
        @NotNull final Status status = Status.NOT_STARTED;
        taskService.changeTaskStatusByIndex(USER_ID_ONE, 5, status);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testChangeTaskStatusByNegativeIndex() {
        @NotNull final Status status = Status.NOT_STARTED;
        taskService.changeTaskStatusByIndex(USER_ID_ONE, -2, status);
    }

    @Test
    public void testAdd() {
        final int expectedNumberOfEntries = taskService.getSize() + 1;
        @NotNull final Task task = new Task();
        task.setName("Test Task");
        task.setUserId(USER_ID_TWO);
        taskService.add(task);
        Assert.assertEquals(expectedNumberOfEntries, taskService.getSize());
    }

    @Test(expected = ModelEmptyException.class)
    public void testAddNull() {
        taskService.add(null);
    }

    @Test
    public void testAddForUser() {
        final int expectedNumberOfEntries = taskService.getSize(USER_ID_TWO) + 1;
        @NotNull final Task task = new Task();
        task.setName("Test Task");
        task.setUserId(USER_ID_TWO);
        taskService.add(task);
        Assert.assertEquals(expectedNumberOfEntries, taskService.getSize(USER_ID_TWO));
    }

    @Test(expected = ModelEmptyException.class)
    public void testAddNullForUser() {
        taskService.add(USER_ID_TWO, null);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testAddForEmptyUser() {
        @NotNull final Task task = new Task();
        task.setUserId(USER_ID_TWO);
        taskService.add("", task);
    }

    @Test
    public void testAddAll() {
        final int expectedNumberOfEntries = taskService.getSize() + 4;
        @NotNull final List<Task> actualTasks = new ArrayList<>();
        for (int i = 1; i <= 4; i++) {
            @NotNull final Task task = new Task();
            task.setName("Test Task " + i);
            task.setUserId(USER_ID_ONE);
            actualTasks.add(task);
        }
        taskService.addAll(actualTasks);
        Assert.assertEquals(expectedNumberOfEntries, taskService.getSize());
    }

    @Test
    public void testAddAllNull() {
        final int expectedNumberOfEntries = taskService.getSize();
        taskService.addAll(null);
        Assert.assertEquals(expectedNumberOfEntries, taskService.getSize());
    }

    @Test
    public void testSet() {
        @NotNull final List<Task> actualTasks = new ArrayList<>();
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final Task task = new Task();
            task.setName("Test Task " + i);
            task.setUserId(USER_ID_TWO);
            actualTasks.add(task);
        }
        taskService.set(actualTasks);
        Assert.assertEquals(NUMBER_OF_ENTRIES, taskService.getSize());
    }

    @Test
    public void testSetNull() {
        final int expectedNumberOfEntries = taskService.getSize();
        taskService.set(null);
        Assert.assertEquals(expectedNumberOfEntries, taskService.getSize());
    }

    @Test
    public void testClearAll() {
        taskService.removeAll();
        Assert.assertEquals(0, taskService.getSize());
    }

    @Test
    public void testClearAllForUser() {
        final int expectedNumberOfEntries = taskService.getSize() - NUMBER_OF_ENTRIES / 2;
        taskService.removeAll(USER_ID_ONE);
        Assert.assertEquals(expectedNumberOfEntries, taskService.getSize());
    }

    @Test
    public void testClear() {
        final int expectedNumberOfEntries = taskService.getSize() - NUMBER_OF_ENTRIES / 2;
        @NotNull final List<Task> taskList = taskService.findAll(USER_ID_TWO);
        taskService.removeAll(taskList);
        Assert.assertEquals(expectedNumberOfEntries, taskService.getSize());
    }

    @Test
    public void testFindAll() {
        @NotNull final List<Task> taskList = taskService.findAll();
        Assert.assertEquals(taskList.size(), taskService.getSize());
    }

    @Test
    public void testFindAllWithComparator() {
        @NotNull final TaskSort sort = TaskSort.BY_NAME;
        @NotNull final List<Task> taskSortList = taskService.findAll(sort.getComparator());
        @NotNull final List<Task> actualTasks = new ArrayList<>();
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final Task task = new Task();
            task.setName("Test " + i);
            actualTasks.add(task);
        }
        actualTasks.add(1, actualTasks.get(NUMBER_OF_ENTRIES - 1));
        actualTasks.remove(NUMBER_OF_ENTRIES);
        Assert.assertEquals(actualTasks, taskSortList);
    }

    @Test
    public void testFindAllWithEmptyComparator() {
        @NotNull final List<Task> taskList = taskService.findAll();
        @NotNull final List<Task> taskSortList = taskService.findAll((Comparator<Task>) null);
        Assert.assertEquals(taskList, taskSortList);
    }

    @Test
    public void testFindAllForUser() {
        @NotNull final List<Task> taskList = taskService.findAll(USER_ID_ONE);
        Assert.assertEquals(NUMBER_OF_ENTRIES / 2, taskList.size());
    }

    @Test(expected = UserIdEmptyException.class)
    public void testFindAllForEmptyUser() {
        taskService.findAll("");
    }

    @Test
    public void testFindAllWithComparatorForUser() {
        @NotNull final TaskSort sort = TaskSort.BY_NAME;
        @NotNull final List<Task> taskSortList = taskService.findAll(USER_ID_TWO, sort.getComparator());
        @NotNull final List<Task> actualTasks = new ArrayList<>();
        for (int i = 1; i <= NUMBER_OF_ENTRIES / 2; i++) {
            @NotNull final Task task = new Task();
            task.setName("Test " + (i + 5));
            actualTasks.add(task);
        }
        actualTasks.add(0, actualTasks.get(NUMBER_OF_ENTRIES / 2 - 1));
        actualTasks.remove(NUMBER_OF_ENTRIES / 2);
        Assert.assertEquals(actualTasks, taskSortList);
    }

    @Test
    public void testFindAllWithEmptyComparatorForUser() {
        @NotNull final List<Task> taskList = taskService.findAll(USER_ID_TWO);
        @NotNull final List<Task> taskSortList = taskService.findAll(USER_ID_TWO, null);
        Assert.assertEquals(taskList, taskSortList);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testFindAllWithComparatorForEmptyUser() {
        @NotNull final TaskSort sort = TaskSort.BY_NAME;
        taskService.findAll("", sort.getComparator());
    }

    @Test
    public void testFindById() {
        @NotNull final Task task = taskService.findAll().get(0);
        @NotNull final String taskId = taskService.findAll().get(0).getId();
        @Nullable final Task actualTask = taskService.findOneById(taskId);
        Assert.assertNotNull(actualTask);
        Assert.assertEquals(task.getName(), actualTask.getName());
        Assert.assertEquals(task.getDescription(), actualTask.getDescription());
        Assert.assertEquals(task.getUserId(), actualTask.getUserId());
    }

    @Test(expected = IdEmptyException.class)
    public void testFindByEmptyId() {
        Assert.assertNull(taskService.findOneById(""));
    }

    @Test
    public void testFindByIdForUser() {
        @NotNull final Task task = taskService.findAll(USER_ID_ONE).get(0);
        @NotNull final String taskId = taskService.findAll(USER_ID_ONE).get(0).getId();
        @Nullable final Task actualTask = taskService.findOneById(USER_ID_ONE, taskId);
        Assert.assertNotNull(actualTask);
        Assert.assertEquals(task.getName(), actualTask.getName());
        Assert.assertEquals(task.getDescription(), actualTask.getDescription());
        Assert.assertEquals(task.getUserId(), actualTask.getUserId());
    }

    @Test(expected = IdEmptyException.class)
    public void testFindByEmptyIdForUser() {
        Assert.assertNull(taskService.findOneById(USER_ID_ONE, ""));
    }

    @Test(expected = UserIdEmptyException.class)
    public void testFindByIdForEmptyUser() {
        @NotNull final String taskId = taskService.findAll(USER_ID_ONE).get(0).getId();
        taskService.findOneById("", taskId);
    }

    @Test
    public void testFindByIdTaskNotFound() {
        @NotNull final String id = UUID.randomUUID().toString();
        Assert.assertNull(taskService.findOneById(id));
    }

    @Test
    public void testFindByIdTaskNotFoundForUser() {
        @NotNull final String id = UUID.randomUUID().toString();
        Assert.assertNull(taskService.findOneById(USER_ID_ONE, id));
    }

    @Test
    public void testFindByIndex() {
        @NotNull final Task task = taskService.findAll().get(0);
        @Nullable final Task actualTask = taskService.findOneByIndex(1);
        Assert.assertNotNull(actualTask);
        Assert.assertEquals(task.getName(), actualTask.getName());
        Assert.assertEquals(task.getDescription(), actualTask.getDescription());
        Assert.assertEquals(task.getUserId(), actualTask.getUserId());
    }

    @Test(expected = IndexIncorrectException.class)
    public void testFindByEmptyIndex() {
        taskService.findOneByIndex(null);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testFindByNegativeIndex() {
        taskService.findOneByIndex(-2);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testFindByIncorrectIndex() {
        taskService.findOneByIndex(taskService.getSize() + 1);
    }

    @Test
    public void testFindByIndexForUser() {
        @NotNull final Task task = taskService.findAll(USER_ID_TWO).get(0);
        @Nullable final Task actualTask = taskService.findOneByIndex(USER_ID_TWO, 1);
        Assert.assertNotNull(actualTask);
        Assert.assertEquals(task.getName(), actualTask.getName());
        Assert.assertEquals(task.getDescription(), actualTask.getDescription());
        Assert.assertEquals(task.getUserId(), actualTask.getUserId());
    }

    @Test(expected = IndexIncorrectException.class)
    public void testFindByEmptyIndexForUser() {
        taskService.findOneByIndex(USER_ID_TWO, null);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testFindByNegativeIndexForUser() {
        taskService.findOneByIndex(USER_ID_TWO, -2);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testFindByIncorrectIndexForUser() {
        taskService.findOneByIndex(USER_ID_TWO, 12);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testFindByIndexForEmptyUser() {
        taskService.findOneByIndex("", 0);
    }

    @Test
    public void testGetSize() {
        final int expectedNumberOfEntries = taskService.getSize() + 1;
        @NotNull final Task task = new Task();
        task.setName("Test Task");
        task.setUserId(USER_ID_ONE);
        taskService.add(task);
        Assert.assertEquals(expectedNumberOfEntries, taskService.getSize());
    }

    @Test
    public void testGetSizeForUser() {
        @NotNull final Task task = new Task();
        task.setName("Test Task");
        task.setUserId(USER_ID_TWO);
        taskService.add(task);
        Assert.assertEquals(NUMBER_OF_ENTRIES / 2 + 1, taskService.getSize(USER_ID_TWO));
    }

    @Test(expected = UserIdEmptyException.class)
    public void testGetSizeForEmptyUser() {
        taskService.getSize("");
    }

    @Test
    public void testIsNotFoundById() {
        @NotNull final String validId = taskService.findAll().get(0).getId();
        @NotNull final String invalidId = UUID.randomUUID().toString();
        Assert.assertFalse(taskService.existsById(invalidId));
        Assert.assertTrue(taskService.existsById(validId));
    }

    @Test(expected = IdEmptyException.class)
    public void testIsNotFoundByEmptyId() {
        taskService.existsById("");
    }

    @Test
    public void testIsNotFoundByIdForUser() {
        @NotNull final String validId = taskService.findAll(USER_ID_ONE).get(0).getId();
        @NotNull final String invalidId = UUID.randomUUID().toString();
        Assert.assertFalse(taskService.existsById(USER_ID_ONE, invalidId));
        Assert.assertTrue(taskService.existsById(USER_ID_ONE, validId));
    }

    @Test(expected = IdEmptyException.class)
    public void testIsNotFoundByEmptyIdForUser() {
        taskService.existsById(USER_ID_ONE, "");
    }

    @Test(expected = UserIdEmptyException.class)
    public void testIsNotFoundForEmptyUser() {
        taskService.existsById("", taskService.findAll(USER_ID_ONE).get(0).getId());
    }

    @Test
    public void testRemove() {
        final int expectedNumberOfEntries = taskService.getSize() - 1;
        @NotNull final Task task = taskService.findAll().get(0);
        taskService.removeOne(task);
        Assert.assertEquals(expectedNumberOfEntries, taskService.getSize());
    }

    @Test(expected = ModelEmptyException.class)
    public void testRemoveNull() {
        taskService.removeOne(null);
    }

    @Test
    public void testRemoveForUser() {
        final int expectedNumberOfEntries = taskService.getSize(USER_ID_TWO) - 1;
        @NotNull final Task task = taskService.findAll(USER_ID_TWO).get(0);
        taskService.removeOne(USER_ID_TWO, task);
        Assert.assertEquals(expectedNumberOfEntries, taskService.getSize(USER_ID_TWO));
    }

    @Test(expected = UserIdEmptyException.class)
    public void testRemoveForEmptyUser() {
        @NotNull final Task task = taskService.findAll().get(0);
        taskService.removeOne("", task);
    }

    @Test(expected = ModelEmptyException.class)
    public void testRemoveNullForUser() {
        taskService.removeOne(USER_ID_TWO, null);
    }

    @Test
    public void testRemoveById() {
        final int expectedNumberOfEntries = taskService.getSize() - 1;
        @NotNull final String taskId = taskService.findAll().get(0).getId();
        taskService.removeOneById(taskId);
        Assert.assertEquals(expectedNumberOfEntries, taskService.getSize());
    }

    @Test
    public void testRemoveByIdForUser() {
        final int expectedNumberOfEntries = taskService.getSize(USER_ID_ONE) - 1;
        @NotNull final String taskId = taskService.findAll(USER_ID_ONE).get(0).getId();
        taskService.removeOneById(USER_ID_ONE, taskId);
        Assert.assertEquals(expectedNumberOfEntries, taskService.getSize(USER_ID_ONE));
    }

    @Test(expected = IdEmptyException.class)
    public void testRemoveByEmptyId() {
        Assert.assertNull(taskService.removeOneById(""));
    }

    @Test(expected = IdEmptyException.class)
    public void testRemoveByEmptyIdForUser() {
        taskService.removeOneById(USER_ID_ONE, "");
    }

    @Test
    public void testRemoveByIdTaskNotFound() {
        @NotNull final String invalidId = UUID.randomUUID().toString();
        Assert.assertNull(taskService.removeOneById(invalidId));
    }

    @Test
    public void testRemoveByIdTaskNotFoundForUser() {
        @NotNull final String invalidId = UUID.randomUUID().toString();
        Assert.assertNull(taskService.removeOneById(USER_ID_ONE, invalidId));
    }

    @Test
    public void testRemoveByIndex() {
        final int expectedNumberOfEntries = taskService.getSize() - 1;
        taskService.removeOneByIndex(1);
        Assert.assertEquals(expectedNumberOfEntries, taskService.getSize());
    }

    @Test(expected = IndexIncorrectException.class)
    public void testRemoveByEmptyIndex() {
        taskService.removeOneByIndex(null);
    }

    @Test
    public void testRemoveByIndexForUser() {
        final int expectedNumberOfEntries = taskService.getSize(USER_ID_TWO) - 1;
        taskService.removeOneByIndex(USER_ID_TWO, 1);
        Assert.assertEquals(expectedNumberOfEntries, taskService.getSize(USER_ID_TWO));
    }

    @Test(expected = IndexIncorrectException.class)
    public void testRemoveByEmptyIndexForUser() {
        taskService.removeOneByIndex(USER_ID_TWO, null);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testRemoveByIndexForEmptyUser() {
        taskService.removeOneByIndex("", 1);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testRemoveByNegativeIndex() {
        taskService.removeOneByIndex(-2);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testRemoveByNegativeIndexForUser() {
        taskService.removeOneByIndex(USER_ID_TWO, -2);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testRemoveByIncorrectIndex() {
        taskService.removeOneByIndex(taskService.getSize() + 1);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testRemoveByIncorrectIndexForUser() {
        taskService.removeOneByIndex(USER_ID_TWO, 5);
    }

    @Test
    public void testFindAllByProjectId() {
        @NotNull final List<Task> tasksOne = taskService.findAll(USER_ID_ONE);
        @NotNull final List<Task> tasks = taskService.findAllByProjectId(USER_ID_ONE, projectOne.getId());
        Assert.assertEquals(tasksOne, tasks);
    }

    @Test
    public void testFindAllByIncorrectProjectId() {
        @NotNull final List<Task> tasks = taskService.findAllByProjectId(USER_ID_ONE, projectTwo.getId());
        Assert.assertEquals(Collections.emptyList(), tasks);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testFindAllByProjectIdForEmptyUser() {
        taskService.findAllByProjectId("", projectOne.getId());
    }

    @After
    public void clearRepository() {
        taskService.set(tasks);
        projectService.removeOne(projectOne);
        projectService.removeOne(projectTwo);
        userService.removeOneById(USER_ID_ONE);
        userService.removeOneById(USER_ID_TWO);
    }

}
