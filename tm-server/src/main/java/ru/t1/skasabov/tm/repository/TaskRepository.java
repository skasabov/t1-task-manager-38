package ru.t1.skasabov.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.skasabov.tm.api.DBConstants;
import ru.t1.skasabov.tm.api.repository.ITaskRepository;
import ru.t1.skasabov.tm.enumerated.Status;
import ru.t1.skasabov.tm.model.Task;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public final class TaskRepository extends AbstractUserOwnedRepository<Task>
        implements ITaskRepository {

    public TaskRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @NotNull
    @Override
    public Task add(@NotNull final String userId, @NotNull final Task task) {
        task.setUserId(userId);
        return add(task);
    }

    @NotNull
    @Override
    protected String getTableName() {
        return DBConstants.TABLE_TASK;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task fetch(@NotNull final ResultSet row) {
        @NotNull final Task task = new Task();
        task.setId(row.getString(DBConstants.COLUMN_ID));
        task.setName(row.getString(DBConstants.COLUMN_NAME));
        task.setCreated(row.getTimestamp(DBConstants.COLUMN_CREATED));
        task.setDescription(row.getString(DBConstants.COLUMN_DESCRIPTION));
        task.setUserId(row.getString(DBConstants.COLUMN_USER_ID));
        task.setProjectId(row.getString(DBConstants.COLUMN_PROJECT_ID));
        task.setStatus(Status.toStatus(row.getString(DBConstants.COLUMN_STATUS)));
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task add(@NotNull final Task task) {
        @NotNull final String sql = String.format(
                "INSERT INTO %s (%s, %s, %s, %s, %s, %s, %s, %s, %s) VALUES (?, ?, ?, ?, ?, ?, ?::status_type, ?, ?)",
                getTableName(), DBConstants.COLUMN_ID, DBConstants.COLUMN_NAME, DBConstants.COLUMN_CREATED,
                DBConstants.COLUMN_DESCRIPTION, DBConstants.COLUMN_USER_ID, DBConstants.COLUMN_PROJECT_ID,
                DBConstants.COLUMN_STATUS, DBConstants.COLUMN_BEGIN_DATE, DBConstants.COLUMN_END_DATE
        );
        try(@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, task.getId());
            statement.setString(2, task.getName());
            statement.setTimestamp(3, new Timestamp(task.getCreated().getTime()));
            statement.setString(4, task.getDescription());
            statement.setString(5, task.getUserId());
            statement.setString(6, task.getProjectId());
            statement.setString(7, task.getStatus().toString());
            @Nullable final Timestamp dateBegin = task.getDateBegin() != null ?
                    new Timestamp(task.getDateBegin().getTime()) : null;
            @Nullable final Timestamp dateEnd = task.getDateEnd() != null ?
                    new Timestamp(task.getDateEnd().getTime()) : null;
            statement.setTimestamp(8, dateBegin);
            statement.setTimestamp(9, dateEnd);
            statement.executeUpdate();
        }
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task update(@NotNull final Task task) {
        @NotNull final String sql = String.format(
                "UPDATE %s SET %s = ?, %s = ?, %s = ?::status_type, %s = ? WHERE %s = ? AND %s = ?",
                getTableName(), DBConstants.COLUMN_NAME, DBConstants.COLUMN_DESCRIPTION,
                DBConstants.COLUMN_STATUS, DBConstants.COLUMN_PROJECT_ID,
                DBConstants.COLUMN_ID, DBConstants.COLUMN_USER_ID);
        try(@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, task.getName());
            statement.setString(2, task.getDescription());
            statement.setString(3, task.getStatus().toString());
            statement.setString(4, task.getProjectId());
            statement.setString(5, task.getId());
            statement.setString(6, task.getUserId());
            statement.executeUpdate();
        }
        return task;
    }

    @NotNull
    @Override
    public Task create(
            @NotNull final String userId,
            @NotNull final String name
    ) {
        @NotNull final Task task = new Task();
        task.setUserId(userId);
        task.setName(name);
        return add(task);
    }

    @NotNull
    @Override
    public Task create(
            @NotNull final String userId,
            @NotNull final String name,
            @NotNull final String description
    ) {
        @NotNull final Task task = new Task();
        task.setUserId(userId);
        task.setName(name);
        task.setDescription(description);
        return add(task);
    }

    @NotNull
    @Override
    public Task create(
            @NotNull final String userId,
            @NotNull final String name,
            @NotNull final String description,
            @NotNull final Date dateBegin,
            @NotNull final Date dateEnd
    ) {
        @NotNull final Task task = new Task();
        task.setUserId(userId);
        task.setName(name);
        task.setDescription(description);
        task.setDateBegin(dateBegin);
        task.setDateEnd(dateEnd);
        return add(task);
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Task> findAllByProjectId(
            @NotNull final String userId,
            @NotNull final String projectId
    ) {
        @NotNull final List<Task> result = new ArrayList<>();
        @NotNull final String sql = String.format(
                "SELECT * FROM %s WHERE %s = ? AND %s = ?",
                getTableName(),
                DBConstants.COLUMN_USER_ID,
                DBConstants.COLUMN_PROJECT_ID
        );
        try(@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            statement.setString(2, projectId);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                result.add(fetch(resultSet));
            }
        }
        return result;
    }

}
