package ru.t1.skasabov.tm.taskmanager.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.t1.skasabov.tm.api.repository.ISessionRepository;
import ru.t1.skasabov.tm.api.repository.IUserRepository;
import ru.t1.skasabov.tm.api.service.IConnectionService;
import ru.t1.skasabov.tm.api.service.IPropertyService;
import ru.t1.skasabov.tm.model.Session;
import ru.t1.skasabov.tm.repository.SessionRepository;
import ru.t1.skasabov.tm.repository.UserRepository;
import ru.t1.skasabov.tm.service.ConnectionService;
import ru.t1.skasabov.tm.service.PropertyService;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class SessionRepositoryTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    @NotNull
    private static String USER_ID_ONE;

    @NotNull
    private static String USER_ID_TWO;

    @NotNull
    private IUserRepository userRepository;

    @NotNull
    private ISessionRepository sessionRepository;

    @NotNull
    private List<Session> sessions;

    @NotNull
    private Connection connection;

    @Before
    @SneakyThrows
    public void initRepository() {
        @NotNull final IPropertyService propertyService = new PropertyService();
        @NotNull final IConnectionService connectionService = new ConnectionService(propertyService);
        connection = connectionService.getConnection();
        connection.setAutoCommit(true);
        userRepository = new UserRepository(connection);
        sessionRepository = new SessionRepository(connection);
        sessions = sessionRepository.findAll();
        sessionRepository.removeAll();
        USER_ID_ONE = userRepository.create("user_one", "user_one").getId();
        USER_ID_TWO = userRepository.create("user_two", "user_two").getId();
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final Session session = new Session();
            if (i <= 5) session.setUserId(USER_ID_ONE);
            else session.setUserId(USER_ID_TWO);
            sessionRepository.add(session);
        }
    }

    @Test
    public void testAdd() {
        final int expectedNumberOfEntries = sessionRepository.getSize() + 1;
        @NotNull final Session session = new Session();
        session.setUserId(USER_ID_TWO);
        sessionRepository.add(session);
        Assert.assertEquals(expectedNumberOfEntries, sessionRepository.getSize());
    }

    @Test
    public void testAddForUser() {
        final int expectedNumberOfEntries = sessionRepository.getSize(USER_ID_TWO) + 1;
        @NotNull final Session session = new Session();
        session.setUserId(USER_ID_TWO);
        sessionRepository.add(session);
        Assert.assertEquals(expectedNumberOfEntries, sessionRepository.getSize(USER_ID_TWO));
    }

    @Test
    public void testAddAll() {
        final int expectedNumberOfEntries = sessionRepository.getSize() + 4;
        @NotNull final List<Session> actualSessions = new ArrayList<>();
        for (int i = 1; i <= 4; i++) {
            @NotNull final Session session = new Session();
            session.setUserId(USER_ID_ONE);
            actualSessions.add(session);
        }
        sessionRepository.addAll(actualSessions);
        Assert.assertEquals(expectedNumberOfEntries, sessionRepository.getSize());
    }

    @Test
    public void testSet() {
        @NotNull final List<Session> actualSessions = new ArrayList<>();
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final Session session = new Session();
            session.setUserId(USER_ID_TWO);
            actualSessions.add(session);
        }
        sessionRepository.set(actualSessions);
        Assert.assertEquals(NUMBER_OF_ENTRIES, sessionRepository.getSize());
    }

    @Test
    public void testClearAll() {
        sessionRepository.removeAll();
        Assert.assertEquals(0, sessionRepository.getSize());
    }

    @Test
    public void testClearAllForUser() {
        final int expectedNumberOfEntries = sessionRepository.getSize() - NUMBER_OF_ENTRIES / 2;
        sessionRepository.removeAll(USER_ID_ONE);
        Assert.assertEquals(expectedNumberOfEntries, sessionRepository.getSize());
    }

    @Test
    public void testClear() {
        final int expectedNumberOfEntries = sessionRepository.getSize() - NUMBER_OF_ENTRIES / 2;
        @NotNull final List<Session> sessionList = sessionRepository.findAll(USER_ID_TWO);
        sessionRepository.removeAll(sessionList);
        Assert.assertEquals(expectedNumberOfEntries, sessionRepository.getSize());
    }

    @Test
    public void testFindAll() {
        @NotNull final List<Session> sessionList = sessionRepository.findAll();
        Assert.assertEquals(sessionList.size(), sessionRepository.getSize());
    }

    @Test
    public void testFindAllForUser() {
        @NotNull final List<Session> sessionList = sessionRepository.findAll(USER_ID_ONE);
        Assert.assertEquals(NUMBER_OF_ENTRIES / 2, sessionList.size());
    }

    @Test
    public void testFindById() {
        @NotNull final Session session = sessionRepository.findAll().get(0);
        @NotNull final String sessionId = sessionRepository.findAll().get(0).getId();
        @Nullable final Session actualSession = sessionRepository.findOneById(sessionId);
        Assert.assertNotNull(actualSession);
        Assert.assertEquals(session.getDate(), actualSession.getDate());
        Assert.assertEquals(session.getRole(), actualSession.getRole());
        Assert.assertEquals(session.getUserId(), actualSession.getUserId());
    }

    @Test
    public void testFindByIdForUser() {
        @NotNull final Session session = sessionRepository.findAll(USER_ID_ONE).get(0);
        @NotNull final String sessionId = sessionRepository.findAll(USER_ID_ONE).get(0).getId();
        @Nullable final Session actualSession = sessionRepository.findOneById(USER_ID_ONE, sessionId);
        Assert.assertNotNull(actualSession);
        Assert.assertEquals(session.getDate(), actualSession.getDate());
        Assert.assertEquals(session.getRole(), actualSession.getRole());
        Assert.assertEquals(session.getUserId(), actualSession.getUserId());
    }

    @Test
    public void testFindByIdSessionNotFound() {
        @NotNull final String id = UUID.randomUUID().toString();
        Assert.assertNull(sessionRepository.findOneById(id));
    }

    @Test
    public void testFindByIdSessionNotFoundForUser() {
        @NotNull final String id = UUID.randomUUID().toString();
        Assert.assertNull(sessionRepository.findOneById(USER_ID_ONE, id));
    }

    @Test
    public void testFindByIndex() {
        @NotNull final Session session = sessionRepository.findAll().get(0);
        @Nullable final Session actualSession = sessionRepository.findOneByIndex(1);
        Assert.assertNotNull(actualSession);
        Assert.assertEquals(session.getDate(), actualSession.getDate());
        Assert.assertEquals(session.getRole(), actualSession.getRole());
        Assert.assertEquals(session.getUserId(), actualSession.getUserId());
    }

    @Test
    public void testFindByIndexSessionNotFound() {
        sessionRepository.removeAll();
        Assert.assertNull(sessionRepository.findOneByIndex(1));
    }

    @Test
    public void testFindByIndexForUser() {
        @NotNull final Session session = sessionRepository.findAll(USER_ID_TWO).get(0);
        @Nullable final Session actualSession = sessionRepository.findOneByIndex(USER_ID_TWO, 1);
        Assert.assertNotNull(actualSession);
        Assert.assertEquals(session.getDate(), actualSession.getDate());
        Assert.assertEquals(session.getRole(), actualSession.getRole());
        Assert.assertEquals(session.getUserId(), actualSession.getUserId());
    }

    @Test
    public void testFindByIndexForUserSessionNotFound() {
        sessionRepository.removeAll();
        Assert.assertNull(sessionRepository.findOneByIndex(USER_ID_ONE, 1));
    }

    @Test
    public void testGetSize() {
        final int expectedNumberOfEntries = sessionRepository.getSize() + 1;
        @NotNull final Session session = new Session();
        session.setUserId(USER_ID_ONE);
        sessionRepository.add(session);
        Assert.assertEquals(expectedNumberOfEntries, sessionRepository.getSize());
    }

    @Test
    public void testGetSizeForUser() {
        @NotNull final Session session = new Session();
        session.setUserId(USER_ID_TWO);
        sessionRepository.add(session);
        Assert.assertEquals(NUMBER_OF_ENTRIES / 2 + 1, sessionRepository.getSize(USER_ID_TWO));
    }

    @Test
    public void testIsNotFoundById() {
        @NotNull final String validId = sessionRepository.findAll().get(0).getId();
        @NotNull final String invalidId = UUID.randomUUID().toString();
        Assert.assertFalse(sessionRepository.existsById(invalidId));
        Assert.assertTrue(sessionRepository.existsById(validId));
    }

    @Test
    public void testIsNotFoundByIdForUser() {
        @NotNull final String validId = sessionRepository.findAll(USER_ID_ONE).get(0).getId();
        @NotNull final String invalidId = UUID.randomUUID().toString();
        Assert.assertFalse(sessionRepository.existsById(USER_ID_ONE, invalidId));
        Assert.assertTrue(sessionRepository.existsById(USER_ID_ONE, validId));
    }

    @Test
    public void testRemove() {
        final int expectedNumberOfEntries = sessionRepository.getSize() - 1;
        @NotNull final Session session = sessionRepository.findAll().get(0);
        sessionRepository.removeOne(session);
        Assert.assertEquals(expectedNumberOfEntries, sessionRepository.getSize());
    }

    @Test
    public void testRemoveForUser() {
        final int expectedNumberOfEntries = sessionRepository.getSize(USER_ID_TWO) - 1;
        @NotNull final Session session = sessionRepository.findAll(USER_ID_TWO).get(0);
        sessionRepository.removeOne(USER_ID_TWO, session);
        Assert.assertEquals(expectedNumberOfEntries, sessionRepository.getSize(USER_ID_TWO));
    }

    @Test
    public void testRemoveById() {
        final int expectedNumberOfEntries = sessionRepository.getSize() - 1;
        @NotNull final String sessionId = sessionRepository.findAll().get(0).getId();
        sessionRepository.removeOneById(sessionId);
        Assert.assertEquals(expectedNumberOfEntries, sessionRepository.getSize());
    }

    @Test
    public void testRemoveByIdForUser() {
        final int expectedNumberOfEntries = sessionRepository.getSize(USER_ID_ONE) - 1;
        @NotNull final String sessionId = sessionRepository.findAll(USER_ID_ONE).get(0).getId();
        sessionRepository.removeOneById(USER_ID_ONE, sessionId);
        Assert.assertEquals(expectedNumberOfEntries, sessionRepository.getSize(USER_ID_ONE));
    }

    @Test
    public void testRemoveByIndex() {
        final int expectedNumberOfEntries = sessionRepository.getSize() - 1;
        sessionRepository.removeOneByIndex(1);
        Assert.assertEquals(expectedNumberOfEntries, sessionRepository.getSize());
    }

    @Test
    public void testRemoveByIndexSessionNotFound() {
        sessionRepository.removeAll();
        Assert.assertNull(sessionRepository.removeOneByIndex(1));
    }

    @Test
    public void testRemoveByIndexForUser() {
        final int expectedNumberOfEntries = sessionRepository.getSize(USER_ID_TWO) - 1;
        sessionRepository.removeOneByIndex(USER_ID_TWO, 1);
        Assert.assertEquals(expectedNumberOfEntries, sessionRepository.getSize(USER_ID_TWO));
    }

    @Test
    public void testRemoveByIndexForUserSessionNotFound() {
        sessionRepository.removeAll();
        Assert.assertNull(sessionRepository.removeOneByIndex(USER_ID_ONE, 1));
    }

    @After
    @SneakyThrows
    public void clearRepository() {
        sessionRepository.set(sessions);
        userRepository.removeOneById(USER_ID_ONE);
        userRepository.removeOneById(USER_ID_TWO);
        connection.close();
    }

}
