package ru.t1.skasabov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.skasabov.tm.api.service.IConnectionService;
import ru.t1.skasabov.tm.api.service.IPropertyService;

import java.sql.Connection;
import java.sql.DriverManager;

public final class ConnectionService implements IConnectionService {

    @NotNull
    private final IPropertyService propertyService;

    public ConnectionService(@NotNull final IPropertyService propertyService) {
        this.propertyService = propertyService;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Connection getConnection() {
        @NotNull final String userName = propertyService.getDatabaseUserName();
        @NotNull final String password = propertyService.getDatabasePassword();
        @NotNull final String url = propertyService.getDatabaseUrl();
        @NotNull final Connection connection = DriverManager.getConnection(url, userName, password);
        connection.setAutoCommit(false);
        return connection;
    }

}
