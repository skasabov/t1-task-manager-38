package ru.t1.skasabov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.skasabov.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;

public interface IUserOwnedService<M extends AbstractUserOwnedModel> extends IService<M> {

    @NotNull
    M add(@Nullable String userId, @Nullable M model);

    @NotNull
    Boolean existsById(@Nullable String userId, @Nullable String id);

    @NotNull
    List<M> findAll(@Nullable String userId);

    @NotNull
    List<M> findAll(@Nullable String userId, @Nullable Comparator<M> comparator);

    @Nullable
    M findOneById(@Nullable String userId, @Nullable String id);

    @Nullable
    M findOneByIndex(@Nullable String userId, @Nullable Integer index);

    int getSize(@Nullable String userId);

    @Nullable
    M removeOneById(@Nullable String userId, @Nullable String id);

    @Nullable
    M removeOneByIndex(@Nullable String userId, @Nullable Integer index);

    @Nullable
    M removeOne(@Nullable String userId, @Nullable M model);

    void removeAll(@Nullable String userId);

}
